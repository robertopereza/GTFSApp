require('dotenv').config()

module.exports = {
    env: {
        SERVICE_HOST: process.env.SERVICE_HOST,
        PLANNER_HOST: process.env.PLANNER_HOST,
    },
}