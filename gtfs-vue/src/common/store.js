const store = {
    state: {
        user: {
            authority: "",
            login: "",
            logged: false,
        },
        loading: {
            badStops: false,
            import: false,
            map: false
        },
    },
    badAccessStops: {
        fromStop: "",
        toStop: "",
        enabled: false
    },
};

export default store;