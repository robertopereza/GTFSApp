import HTTP from "@/common/http";

const resource = "pbf"

export default {
    async findAll() {
        const response = await HTTP.get(resource);
        return response.data;
    },
    async importPbf(file) {
        return (await HTTP.post(resource + `/importPbf`, file)).data;
    },
    async buildPbf(fileName) {
        return (await HTTP.post(resource + `/buildPbf`, fileName)).data;
    },
    async buildedPbf() {
        const response = await HTTP.get(resource + "/builded");
        return response.data;
    },
    async removePbf(fileName) {
        const response = await HTTP.delete(resource + `/remove`, { data: fileName });
        return response.data;
    }
}