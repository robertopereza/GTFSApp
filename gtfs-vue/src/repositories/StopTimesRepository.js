import HTTP from "@/common/http"

const resource = "stoptimes"

export default {
    async findAll() {
        const response = await HTTP.get(resource);
        return response.data;
    },
    async findByStopAndRoute(stopId, routeId) {
        const response = await HTTP.get(resource + "/findByStopAndRoute", {
            params: {
                stopId: stopId,
                routeId: routeId
            }
        })
        return response.data;;
    },
    async findByStop(stopId) {
        const response = await HTTP.get(resource + "/findByStop", {
            params: {
                stopId: stopId
            }
        })
        console.log(response.data);
        return response.data;;
    }
};