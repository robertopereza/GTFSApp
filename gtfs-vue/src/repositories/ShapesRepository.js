import HTTP from "@/common/http"

const resource = "shapes"

export default {
    async findAll() {
        const response = await HTTP.get(resource);
        return response.data;
    },
    async findByRoute(routeId) {
        const response = await HTTP.get(resource + "/findByRoute", {
            params: {
                routeId: routeId
            }
        })
        return response.data;;
    }
};