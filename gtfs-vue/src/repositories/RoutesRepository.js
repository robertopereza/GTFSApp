import HTTP from "@/common/http"

const resource = "routes"

export default {
    async findAll() {
        const response = await HTTP.get(resource);
        return response.data;
    },

    async findByStop(stopId) {
        const response = await HTTP.get(resource + "/findByStop", {
            params: {
                stopId: stopId
            }
        })
        return response.data;;
    }
};