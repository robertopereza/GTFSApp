import HTTP from "@/common/http";

const resource = "gtfs"

export default {
    async findAll() {
        const response = await HTTP.get(resource);
        return response.data;
    },
    async importGtfs(file) {
        return (await HTTP.post(resource + `/importGtfs`, file)).data;
    },
    async buildGtfs(files) {
        return (await HTTP.post(resource + `/buildGtfs`, files)).data;
    },
    async buildedGtfs() {
        const response = await HTTP.get(resource + "/builded");
        return response.data;
    },
    async removeGtfs(fileName) {
        const response = await HTTP.delete(resource + `/remove`, { data: fileName });
        return response.data;
    }

}