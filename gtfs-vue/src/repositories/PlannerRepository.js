import HTTP from "@/common/http"

const resource = "planner"

export default {
    async planRoute(start, end) {
        const response = await HTTP.get(resource + "/planRoute", {
            params: {
                "start": start.lat + ', ' + start.lng,
                "end": end.lat + ', ' + end.lng,
            }
        });
        return response.data;
    },

    async findIsochrones(stopCoordinates, isochroneTimes) {
        var time1 = isochroneTimes[0] * 60;
        var time2 = isochroneTimes[1] * 60;
        var time3 = isochroneTimes[2] * 60;
        const response = await HTTP.get(resource + "/findIsochrones", {
            params: {
                "fromPlace": stopCoordinates[0] + ', ' + stopCoordinates[1],
                "cutoffSec1": time1.toString(),
                "cutoffSec2": time2.toString(),
                "cutoffSec3": time3.toString(),
            }
        });
        return response.data;
    },
};