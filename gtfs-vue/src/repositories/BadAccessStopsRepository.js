import HTTP from "@/common/http"

const resource = "badAccessStops"

export default {
    async findAll() {
        const response = await HTTP.get(resource);
        return response.data;
    },

    async calculateBadAccessStops(meters, time) {
        const response = await HTTP.get(resource + "/calculateStops", {
            params: {
                meters: meters,
                time: time
            }
        });
        return response.data;
    }
};