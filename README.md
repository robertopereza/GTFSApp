# Name

---

GTFS-Madrid

# Installation

---

## Host

En gtfs/src/main/resources/application.yml modificamos la propiedad "clientHost" con la dirección en la que se ejecutará el cliente.

En gtfs-vue/.env modificamos las variables de entorno "VUE_APP_SERVICE_HOST" y "VUE_APP_PLANNER_HOST" con la dirección en la que se ejecuta el servicio. Mantener el puerto 5555 de "VUE_APP_PLANNER_HOST". 

## Directorios

1. Asegurarse de que existan los directorios, o otros directorios con una estructura similar:

C:/Users/Public/Documents/Gtfs

C:/Users/Public/Documents/Gtfs/builded

C:/Users/Public/Documents/Gtfs/files

2. En gtfs/src/main/resources/application.yml modificamos las propiedades rootPath, buildedPath y filesPath con los directorios anteriores.

3. Copiar los ficheros otp-1.5.0-shaded.jar, drop_tablas.sql, geo_tablas.sql, stg_routes.sql y tablas.sql al directorio C:/Users/Public/Documents/Gtfs

NOTA: Si no se despliega en Windows, hay que cambiar las rutas del fichero gtfs/src/main/resources/application.yml. En el fichero, cambiar las rutas de la properties rootPath, buildedPath y filesPath. Por último copiar los ficheros al directorio asignado en rootPath.

## Base de datos

1. Crear una base de datos en postgres.

2. Ejecutar el siguiente comando en la base de datos:

`create extension postgis;`

## Backend

1. En /gtfs ejecutar:

`mvn install`

2. En /gtfs/src/main/resources/application.yml modificar las propiedades database, username y password por las correspondientes de la base de datos creada.

3. Ejecutar el servicio desde el directorio raíz con el comando:

`mvn spring-boot:run`

## Frontend

1. En /gtfs-vue ejecutar:

`npm install`

`npm run serve`

Debería estar funcionando ya la aplicación en localhost:1234

# Usage

---

Logearse como admin con el usuario/contraseña pepe/pepe.
En la pestaña "Importar modelo" examinar e importar los ficheros .zip que se quieran cargar en base de datos. (El que menos tarda es gtfs-metro-madrid.zip) 
Una vez importados, cargar los datos haciendo click en el icono debajo de la tabla al lado del +.
En la 2ª tabla importamos el archivo .pbf y lo cargamos haciendo click en el icono que aparece en la fila de la tabla.
Una vez cargados podemos interactuar con el mapa. 

OJO al cargar el fichero PBF porque se crea un proceso java en segundo plano de OpenTripPlanner. Al cerrar la aplicación comprobar que también se eliminó el proceso.