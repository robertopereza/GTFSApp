package es.udc.gtfs.model.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Levels {
    @Id
    @Column(name = "level_id")
    private String id;

    @Column(name = "level_index")
    private Float levelIndex;

    @Column(name = "level_name")
    private String levelName;

    public Levels() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Float getLevelIndex() {
        return levelIndex;
    }

    public void setLevelIndex(Float levelIndex) {
        this.levelIndex = levelIndex;
    }

    public String getLevelName() {
        return levelName;
    }

    public void setLevelName(String levelName) {
        this.levelName = levelName;
    }

    
}
