package es.udc.gtfs.model.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@IdClass(FrequenciesCompositeKey.class)
public class Frequencies {
    @Id
    @JoinColumn(name = "trip_id", referencedColumnName = "trip_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private Trips tripId;

    @Id
    @Column(name = "start_time")
    private String startTime;

    @Id
    @Column(name = "end_time")
    private String endTime;

    @Column(name = "headway_secs")
    private Integer headway_secs;

    @Column(name = "exact_times")
    private Integer exact_times;

    public Frequencies() {
    }


    public Trips getTripId() {
        return tripId;
    }

    public void setTripId(Trips tripId) {
        this.tripId = tripId;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public Integer getHeadway_secs() {
        return headway_secs;
    }

    public void setHeadway_secs(Integer headway_secs) {
        this.headway_secs = headway_secs;
    }

    public Integer getExact_times() {
        return exact_times;
    }

    public void setExact_times(Integer exact_times) {
        this.exact_times = exact_times;
    }

    
}
