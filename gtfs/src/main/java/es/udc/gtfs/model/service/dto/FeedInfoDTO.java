package es.udc.gtfs.model.service.dto;

import java.time.LocalDateTime;

import es.udc.gtfs.model.domain.FeedInfo;

public class FeedInfoDTO {
    private String publisherName;

    private String publisherUrl;

    private String lang;

    private LocalDateTime startDate;

    private LocalDateTime endDate;

    private String version;

    public FeedInfoDTO() {
    }

    public FeedInfoDTO(FeedInfo feedInfo) {
        this.publisherName = feedInfo.getPublisherName();
        this.publisherUrl = feedInfo.getPublisherUrl();
        this.lang = feedInfo.getLang();
        this.startDate = feedInfo.getStartDate();
        this.endDate = feedInfo.getEndDate();
        this.version = feedInfo.getVersion();
    }

    public String getPublisherName() {
        return publisherName;
    }

    public void setPublisherName(String publisherName) {
        this.publisherName = publisherName;
    }

    public String getPublisherUrl() {
        return publisherUrl;
    }

    public void setPublisherUrl(String publisherUrl) {
        this.publisherUrl = publisherUrl;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    
}
