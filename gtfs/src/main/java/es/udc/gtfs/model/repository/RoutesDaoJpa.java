package es.udc.gtfs.model.repository;

import java.util.List;

import javax.persistence.TypedQuery;

import org.springframework.dao.support.DataAccessUtils;
import org.springframework.stereotype.Repository;

import es.udc.gtfs.model.domain.Routes;
import es.udc.gtfs.model.repository.util.GenericDaoJpa;

@Repository
public class RoutesDaoJpa extends GenericDaoJpa implements RoutesDao {
    @Override
    public List<Routes> findAll() {
        return entityManager.createQuery("from Routes", Routes.class).getResultList();
    }

    // Recupera las líneas que pasan por una parada
    @Override
    public List<Routes> findByStop(String stopId) {
        TypedQuery<Routes> query = entityManager.createQuery("SELECT NEW es.udc.gtfs.model.domain.Routes(r.id, r.agencyId, r.shortName, r.longName, r.desc, r.type, r.url, r.color, r.textColor)  FROM Routes r"
        + " JOIN Trips t ON r.id = t.routeId"
        + " JOIN StopTimes st ON t.id = st.tripId"
        + " JOIN Stops s ON st.stopId = s.id"
        + " WHERE s.id = :stopId"
        + " GROUP BY r.id, r.agencyId, r.shortName, r.longName, r.desc, r.type, r.url, r.color, r.textColor", Routes.class).setParameter("stopId", stopId);

        return query.getResultList();
    }

    @Override
    public Routes findById(String id) {
        TypedQuery<Routes> route = entityManager.createQuery("from Routes r where r.id = :id", Routes.class).setParameter("id", id);
        return DataAccessUtils.singleResult(route.getResultList());
    }

}
