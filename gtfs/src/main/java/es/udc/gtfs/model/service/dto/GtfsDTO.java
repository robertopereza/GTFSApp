package es.udc.gtfs.model.service.dto;

import java.util.List;

import javax.validation.constraints.NotEmpty;

import es.udc.gtfs.model.domain.Gtfs;
import es.udc.gtfs.model.domain.RouteType;

public class GtfsDTO {
    private Long id;

    @NotEmpty
    private String name;

    @NotEmpty
    private List<RouteType> routeType;


    public GtfsDTO() {
    }

    public GtfsDTO(Gtfs gtfs) {
        this.id = gtfs.getId();
        this.name = gtfs.getName();
        this.routeType = gtfs.getRouteType();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<RouteType> getRouteType() {
        return routeType;
    }

    public void setRouteType(List<RouteType> routeType) {
        this.routeType = routeType;
    }
    
}
