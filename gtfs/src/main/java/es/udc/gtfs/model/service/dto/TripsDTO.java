package es.udc.gtfs.model.service.dto;

import es.udc.gtfs.model.domain.Calendar;
import es.udc.gtfs.model.domain.Routes;
import es.udc.gtfs.model.domain.Shapes;
import es.udc.gtfs.model.domain.Trips;

public class TripsDTO {
    private Routes routeId;

    private Calendar serviceId;

    private String id;

    private String headsign;

    private String shortName;

    private String directionId;

    private String blockId;

    private Shapes shapeId;

    private String wheelchairAccessible;

    public TripsDTO() {
    }

    public TripsDTO(Trips trips) {
        this.routeId = trips.getRouteId();
        this.serviceId = trips.getServiceId();
        this.id = trips.getId();
        this.headsign = trips.getHeadsign();
        this.shortName = trips.getShortName();
        this.directionId = trips.getDirectionId();
        this.blockId = trips.getBlockId();
        this.shapeId = trips.getShapeId();
        this.wheelchairAccessible = trips.getWheelchairAccessible();
    }

    public Routes getRouteId() {
        return routeId;
    }

    public void setRouteId(Routes routeId) {
        this.routeId = routeId;
    }

    public Calendar getServiceId() {
        return serviceId;
    }

    public void setServiceId(Calendar serviceId) {
        this.serviceId = serviceId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHeadsign() {
        return headsign;
    }

    public void setHeadsign(String headsign) {
        this.headsign = headsign;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getDirectionId() {
        return directionId;
    }

    public void setDirectionId(String directionId) {
        this.directionId = directionId;
    }

    public String getBlockId() {
        return blockId;
    }

    public void setBlockId(String blockId) {
        this.blockId = blockId;
    }

    public Shapes getShapeId() {
        return shapeId;
    }

    public void setShapeId(Shapes shapeId) {
        this.shapeId = shapeId;
    }

    public String getWheelchairAccessible() {
        return wheelchairAccessible;
    }

    public void setWheelchairAccessible(String wheelchairAccessible) {
        this.wheelchairAccessible = wheelchairAccessible;
    }

    
}
