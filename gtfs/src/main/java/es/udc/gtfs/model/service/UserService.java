package es.udc.gtfs.model.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.udc.gtfs.model.domain.User;
import es.udc.gtfs.model.domain.UserAuthority;
import es.udc.gtfs.model.exception.NotFoundException;
import es.udc.gtfs.model.exception.OperationNotAllowed;
import es.udc.gtfs.model.exception.UserLoginExistsException;
import es.udc.gtfs.model.repository.UserDao;
import es.udc.gtfs.model.security.SecurityUtils;
import es.udc.gtfs.model.service.dto.UserDTO;

@Service
@Transactional(readOnly = true, rollbackFor = Exception.class)
public class UserService {
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserDao userDAO;

    public List<UserDTO> findAll() {
        return userDAO.findAll().stream().map(user -> new UserDTO(user)).collect(Collectors.toList());
    }

    @Transactional(readOnly = false)
    public void registerUser(String login, String password) throws UserLoginExistsException {
        registerUser(login, password, UserAuthority.USER);
    }

    @Transactional(readOnly = false)
    public void registerUser(String login, String password, UserAuthority authority) throws UserLoginExistsException {
        if (userDAO.findByLogin(login) != null) {
            throw new UserLoginExistsException(login);
        }

        User user = new User();
        String encryptedPassword = passwordEncoder.encode(password);

        user.setLogin(login);
        user.setPassword(encryptedPassword);
        user.setAuthority(authority);

        userDAO.create(user);
    }

    @Transactional(readOnly = false)
    public void removeUser(Long id) throws NotFoundException, OperationNotAllowed {
        User user = userDAO.findById(id);
        if (user == null) {
            throw new NotFoundException(id.toString(), User.class);
        }
        UserDTO currentUser = getCurrentUserWithAuthority();
        if (currentUser.getId().equals(user.getId())) {
            throw new OperationNotAllowed("El usuario no puede eliminarse a sí mismo");
        }
        userDAO.delete(user);
    }

    @Transactional(readOnly = false)
    public void updateUser(Long id, UserAuthority authority) throws NotFoundException {
        User user = userDAO.findById(id);
        if (user == null) {
            throw new NotFoundException(id.toString(), User.class);
        }
        user.setAuthority(authority);
        userDAO.update(user);
    }

    public UserDTO getCurrentUserWithAuthority() {
        String currentUserLogin = SecurityUtils.getCurrentUserLogin();
        if (currentUserLogin != null) {
            return new UserDTO(userDAO.findByLogin(currentUserLogin));
        }
        return null;
    }
}
