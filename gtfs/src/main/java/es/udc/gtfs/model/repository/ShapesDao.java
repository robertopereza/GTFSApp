package es.udc.gtfs.model.repository;

import java.util.List;

import es.udc.gtfs.model.domain.Shapes;

public interface ShapesDao {
    List<Shapes> findAll();

    List<Shapes> findByRoute(String routeId);

    void updateShapesRoutes();
}
