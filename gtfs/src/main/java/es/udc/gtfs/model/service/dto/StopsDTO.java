package es.udc.gtfs.model.service.dto;

import java.util.Set;

import org.locationtech.jts.geom.Point;

import es.udc.gtfs.model.domain.Stops;
import es.udc.gtfs.model.domain.StopsRouteType;

public class StopsDTO {
    private String id;

    private String code;

    private String name;

    private String desc;

    private Point point;

    private String zoneId;

    private String url;

    private String locationType;

    private String parentStation;

    private String timezone;

    private String wheelchairBoarding;

    private Set<StopsRouteType> stopsRouteType;

    public StopsDTO() {
    }

    public StopsDTO(Stops stops) {
        this.id = stops.getId();
        this.code = stops.getCode();
        this.name = stops.getName();
        this.desc = stops.getDesc();
        this.point = stops.getPoint();
        this.zoneId = stops.getZoneId();
        this.url = stops.getUrl();
        this.locationType = stops.getLocationType();
        this.parentStation = stops.getParentStation();
        this.timezone = stops.getTimezone();
        this.wheelchairBoarding = stops.getWheelchairBoarding();
        this.stopsRouteType = stops.getStopsRouteType();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Point getPoint() {
        return point;
    }

    public void setPoint(Point point) {
        this.point = point;
    }

    public String getZoneId() {
        return zoneId;
    }

    public void setZoneId(String zoneId) {
        this.zoneId = zoneId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getLocationType() {
        return locationType;
    }

    public void setLocationType(String locationType) {
        this.locationType = locationType;
    }

    public String getParentStation() {
        return parentStation;
    }

    public void setParentStation(String parentStation) {
        this.parentStation = parentStation;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getWheelchairBoarding() {
        return wheelchairBoarding;
    }

    public void setWheelchairBoarding(String wheelchairBoarding) {
        this.wheelchairBoarding = wheelchairBoarding;
    }

    public Set<StopsRouteType> getStopsRouteType() {
        return stopsRouteType;
    }

    public void setStopsRouteType(Set<StopsRouteType> stopsRouteType) {
        this.stopsRouteType = stopsRouteType;
    }

}
