package es.udc.gtfs.model.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import es.udc.gtfs.model.domain.CalendarDates;
import es.udc.gtfs.model.repository.util.GenericDaoJpa;

@Repository
public class CalendarDatesDaoJpa extends GenericDaoJpa implements CalendarDatesDao {
    @Override
    public List<CalendarDates> findAll() {
        return entityManager.createQuery("from CalendarDates", CalendarDates.class).getResultList();
    }
}
