package es.udc.gtfs.model.domain;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@IdClass(FareRulesCompositeKey.class)
public class FareRules {
    @Id
    @JoinColumn(name = "fare_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private FareAttributes fareId;

    @Id
    @JoinColumn(name = "route_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private Routes routeId;

    @JoinColumn(name = "origin_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private Stops originId;

    @JoinColumn(name = "destination_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private Stops destinationId;

    @JoinColumn(name = "contains_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private Stops containsId;

    public FareRules() {
    }

    public FareAttributes getFareId() {
        return fareId;
    }

    public void setFareId(FareAttributes fareId) {
        this.fareId = fareId;
    }

    public Routes getRouteId() {
        return routeId;
    }

    public void setRouteId(Routes routeId) {
        this.routeId = routeId;
    }

    public Stops getOriginId() {
        return originId;
    }

    public void setOriginId(Stops originId) {
        this.originId = originId;
    }

    public Stops getDestinationId() {
        return destinationId;
    }

    public void setDestinationId(Stops destinationId) {
        this.destinationId = destinationId;
    }

    public Stops getContainsId() {
        return containsId;
    }

    public void setContainsId(Stops containsId) {
        this.containsId = containsId;
    }

    
}
