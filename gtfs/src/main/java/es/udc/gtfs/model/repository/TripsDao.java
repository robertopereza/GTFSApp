package es.udc.gtfs.model.repository;

import java.util.List;

import es.udc.gtfs.model.domain.Routes;
import es.udc.gtfs.model.domain.Trips;

public interface TripsDao {
    List<Trips> findAll();

    List<Trips> findByRouteId(Routes routeId);
}
