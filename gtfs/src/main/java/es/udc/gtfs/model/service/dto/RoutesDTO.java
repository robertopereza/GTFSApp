package es.udc.gtfs.model.service.dto;

import es.udc.gtfs.model.domain.Agency;
import es.udc.gtfs.model.domain.Routes;

public class RoutesDTO {
    private String id;

    private Agency agencyId;

    private String shortName;

    private String longName;

    private String desc;

    private String type;

    private String url;

    private String color;

    private String textColor;

    public RoutesDTO() {
    }

    public RoutesDTO(Routes routes) {
        this.id = routes.getId();
        this.agencyId = routes.getAgencyId();
        this.shortName = routes.getShortName();
        this.longName = routes.getLongName();
        this.desc = routes.getDesc();
        this.type = routes.getType();
        this.url = routes.getUrl();
        this.color = routes.getColor();
        this.textColor = routes.getTextColor();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Agency getAgencyId() {
        return agencyId;
    }

    public void setAgencyId(Agency agencyId) {
        this.agencyId = agencyId;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getLongName() {
        return longName;
    }

    public void setLongName(String longName) {
        this.longName = longName;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getTextColor() {
        return textColor;
    }

    public void setTextColor(String textColor) {
        this.textColor = textColor;
    }

    
}
