package es.udc.gtfs.model.domain;

public enum UserAuthority {
    USER, MUNICIPAL, ADMIN
}
