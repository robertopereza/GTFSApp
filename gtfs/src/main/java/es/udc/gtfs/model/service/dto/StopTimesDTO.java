package es.udc.gtfs.model.service.dto;

import es.udc.gtfs.model.domain.StopTimes;
import es.udc.gtfs.model.domain.Stops;
import es.udc.gtfs.model.domain.Trips;

public class StopTimesDTO {
    private Trips tripId;

    private String arrivalTime;

    private String departureTime;

    private Stops stopId;

    private String sequence;

    private String headsign;

    private String pickupType;

    private String dropOffType;

    private Float shapeDistTraveled;

    public StopTimesDTO() {
    }

    public StopTimesDTO(StopTimes stopTimes) {
        this.tripId = stopTimes.getTripId();
        this.arrivalTime = stopTimes.getArrivalTime();
        this.departureTime = stopTimes.getDepartureTime();
        this.stopId = stopTimes.getStopId();
        this.sequence = stopTimes.getSequence();
        this.headsign = stopTimes.getHeadsign();
        this.pickupType = stopTimes.getPickupType();
        this.dropOffType = stopTimes.getDropOffType();
        this.shapeDistTraveled = stopTimes.getShapeDistTraveled();
    }

    public Trips getTripId() {
        return tripId;
    }

    public void setTripId(Trips tripId) {
        this.tripId = tripId;
    }

    public String getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(String arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public String getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(String departureTime) {
        this.departureTime = departureTime;
    }

    public Stops getStopId() {
        return stopId;
    }

    public void setStopId(Stops stopId) {
        this.stopId = stopId;
    }

    public String getSequence() {
        return sequence;
    }

    public void setSequence(String sequence) {
        this.sequence = sequence;
    }

    public String getHeadsign() {
        return headsign;
    }

    public void setHeadsign(String headsign) {
        this.headsign = headsign;
    }

 
    public String getPickupType() {
        return pickupType;
    }

    public void setPickupType(String pickupType) {
        this.pickupType = pickupType;
    }

    public String getDropOffType() {
        return dropOffType;
    }

    public void setDropOffType(String dropOffType) {
        this.dropOffType = dropOffType;
    }

    public Float getShapeDistTraveled() {
        return shapeDistTraveled;
    }

    public void setShapeDistTraveled(Float shapeDistTraveled) {
        this.shapeDistTraveled = shapeDistTraveled;
    }

    
}
