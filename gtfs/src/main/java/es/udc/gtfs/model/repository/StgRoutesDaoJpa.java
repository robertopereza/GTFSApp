package es.udc.gtfs.model.repository;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import es.udc.gtfs.config.Properties;
import es.udc.gtfs.model.domain.Gtfs;
import es.udc.gtfs.model.domain.RouteType;
import es.udc.gtfs.model.repository.util.GenericDaoJpa;
import es.udc.gtfs.model.repository.util.ROUTE_TYPE;

@Repository
public class StgRoutesDaoJpa extends GenericDaoJpa implements StgRoutesDao {
    @Autowired
    private Properties properties;

    @Override
    public void loadStageRoutesTable() throws IOException {
        File script = new File(properties.getRootPath() + "/stg_routes.sql");
        
        // Script de creación de tablas stage
        String stg = FileUtils.readFileToString(script, "UTF-8");
        
        // Ejecutar script de tablas stage para crear las tablas
        Query queryStg = entityManager.createNativeQuery(stg);
        queryStg.executeUpdate();

        File file = new File(properties.getFilesPath() + "/routes.txt");
        BufferedReader br = new BufferedReader(
                    new InputStreamReader(new FileInputStream(file.getAbsolutePath()), "UTF-8"));
        // Por si hay un caracter especial de UTF-8
        br.mark(1);
        if (br.read() != 0xFEFF)
            br.reset();
        // Las columnas de la tabla que va a insertar el fichero GTFS
        String columns = br.readLine();
        br.close();
            
        // Iteramos las filas de los ficheros para insertar los datos en las tablas stage 
        
        LineIterator it = FileUtils.lineIterator(file, "UTF-8");
        
        String tableName = file.getName().substring(0, file.getName().length() - 4); // Quitar el .txt
        System.out.println("Cargando tabla stg_" + tableName + "...");
        try {
            Query insertQuery = null;
            String line = it.nextLine();
            String insert = "INSERT INTO stg_" + tableName 
            + "(" + columns + ")" + " " 
            + "VALUES(";
            
            
            while (it.hasNext()) {
                line = it.nextLine();
                line = line.replaceAll("'", "''");
                /*if (!tableName.equals("frequencies") || !tableName.equals("stop_times"))
                    line = line.replaceAll(":", "\\\\:");*/
                line = line.replaceAll(",", "','");
                line = "'" + line + "'";
                line = line.replaceAll("''", "null");
                insertQuery = entityManager.createNativeQuery(insert + line + ")");
                //System.out.println(insert + line + ")");
                insertQuery.executeUpdate();
                
            }
        } finally {
            it.close();
        }
    }

    @Override
    public List<RouteType> findStageRouteType(Gtfs gtfs) {
        Query results = entityManager.createQuery("SELECT distinct(type) FROM StgRoutes sr");
        List<RouteType> routeTypes = new ArrayList<>();
        for (Object result : results.getResultList()) {
            RouteType rType = new RouteType(ROUTE_TYPE.getValue(Integer.parseInt(result.toString())).name());
            rType.setGtfsId(gtfs);;
            routeTypes.add(rType);
        }
        return routeTypes;
    }

}
