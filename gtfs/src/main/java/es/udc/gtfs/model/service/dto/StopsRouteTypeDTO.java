package es.udc.gtfs.model.service.dto;

import javax.validation.constraints.NotEmpty;

import es.udc.gtfs.model.domain.Stops;
import es.udc.gtfs.model.domain.StopsRouteType;

public class StopsRouteTypeDTO {
    private Long id;

    @NotEmpty
    private Stops stopId;

    @NotEmpty
    private String stopsRouteType;

    public StopsRouteTypeDTO() {
    }

    public StopsRouteTypeDTO(StopsRouteType stopsRouteType) {
        this.id = stopsRouteType.getId();
        this.stopId = stopsRouteType.getStopId();
        this.stopsRouteType = stopsRouteType.getStopsRouteType();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Stops getStopId() {
        return stopId;
    }

    public void setStopId(Stops stopId) {
        this.stopId = stopId;
    }

    public String getStopsRouteType() {
        return stopsRouteType;
    }

    public void setStopsRouteType(String stopsRouteType) {
        this.stopsRouteType = stopsRouteType;
    }

    
}
