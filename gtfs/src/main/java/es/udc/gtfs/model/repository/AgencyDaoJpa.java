package es.udc.gtfs.model.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import es.udc.gtfs.model.domain.Agency;
import es.udc.gtfs.model.repository.util.GenericDaoJpa;

@Repository
public class AgencyDaoJpa extends GenericDaoJpa implements AgencyDao {
    @Override
    public List<Agency> findAll() {
        return entityManager.createQuery("from Agency", Agency.class).getResultList();
    }
}
