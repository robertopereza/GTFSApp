package es.udc.gtfs.model.service.dto;

import javax.validation.constraints.NotEmpty;

import es.udc.gtfs.model.domain.Pbf;

public class PbfDTO {
    private Long id;

    @NotEmpty
    private String name;

    public PbfDTO() {
    }

    public PbfDTO(Pbf pbf) {
        this.id = pbf.getId();
        this.name = pbf.getName();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

   

    
}
