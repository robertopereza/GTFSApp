package es.udc.gtfs.model.service;



import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import es.udc.gtfs.config.Properties;

@Service
@Transactional(readOnly = true)
public class PlannerService {
    @Autowired
    private Properties properties;

    public String planRoute(String start, String end) {
        RestTemplate restTemplate = new RestTemplate();
        Map<String, String> uriParams = new HashMap<String, String>();
        uriParams.put("fromPlace", start);
        uriParams.put("toPlace", end);
        uriParams.put("mode", "WALK, BUS, SUBWAY");
        uriParams.put("numItineraries", "3");
        uriParams.put("pathComparator", "duration");
        ResponseEntity<String> response = restTemplate.getForEntity(properties.getPlannerUri(), String.class, uriParams);
        return response.getBody();
    }

    public String findIsochrones(String fromPlace, String cutoffSec1, String cutoffSec2, String cutoffSec3) {
        RestTemplate restTemplate = new RestTemplate();
        Map<String, String> uriParams = new HashMap<String, String>();
        uriParams.put("fromPlace", fromPlace);
        uriParams.put("mode", "WALK, BUS, SUBWAY");
        uriParams.put("cutoffSec1", cutoffSec1);
        uriParams.put("cutoffSec2", cutoffSec2);
        uriParams.put("cutoffSec3", cutoffSec3);
        ResponseEntity<String> response = restTemplate.getForEntity(properties.getIsochroneUri(), String.class, uriParams);
        return response.getBody();
    }
}
