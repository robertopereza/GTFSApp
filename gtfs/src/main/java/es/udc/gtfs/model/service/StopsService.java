package es.udc.gtfs.model.service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.udc.gtfs.model.domain.Stops;
import es.udc.gtfs.model.repository.StopsDao;
import es.udc.gtfs.model.service.dto.StopsDTO;

@Service
@Transactional(readOnly = true)
public class StopsService {

    @Autowired
    private StopsDao stopsDAO;

    public List<StopsDTO> findAll() {
        return stopsDAO.findAll().stream().sorted(Comparator.comparing(Stops::getId)).map(StopsDTO::new)
                .collect(Collectors.toList());
    }

}
