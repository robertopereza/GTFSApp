package es.udc.gtfs.model.exception;

public class OperationNotAllowed extends ModelException {

  public OperationNotAllowed(String msg) {
    super("Operación no permitida: " + msg);
  }
}
