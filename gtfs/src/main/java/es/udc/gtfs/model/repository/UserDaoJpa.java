package es.udc.gtfs.model.repository;

import java.util.List;

import javax.persistence.TypedQuery;

import org.springframework.dao.support.DataAccessUtils;
import org.springframework.stereotype.Repository;

import es.udc.gtfs.model.domain.User;
import es.udc.gtfs.model.repository.util.GenericDaoJpa;

@Repository
public class UserDaoJpa extends GenericDaoJpa implements UserDao {
    @Override
    public List<User> findAll() {
      return entityManager.createQuery("from User", User.class).getResultList();
    }
  
    @Override
    public User findById(Long id) {
      return entityManager.find(User.class, id);
    }
  
    @Override
    public User findByLogin(String login) {
      TypedQuery<User> query = entityManager.createQuery("from User u where u.login = :login", User.class)
          .setParameter("login", login);
      return DataAccessUtils.singleResult(query.getResultList());
    }
  
    @Override
    public void create(User user) {
      entityManager.persist(user);
    }
  
    @Override
    public void update(User user) {
      entityManager.merge(user);
    }
  
    @Override
    public void delete(User user) {
      entityManager.remove(user);
    }
}
