package es.udc.gtfs.model.repository;

import java.util.List;

import es.udc.gtfs.model.domain.CalendarDates;

public interface CalendarDatesDao {
    List<CalendarDates> findAll();
}
