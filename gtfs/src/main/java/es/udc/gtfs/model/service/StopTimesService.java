package es.udc.gtfs.model.service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.udc.gtfs.model.domain.StopTimes;
import es.udc.gtfs.model.domain.Stops;
import es.udc.gtfs.model.repository.StopTimesDao;
import es.udc.gtfs.model.service.dto.StopTimesDTO;

@Service
@Transactional(readOnly = true)
public class StopTimesService {
    @Autowired
    private StopTimesDao stopTimesDAO;

    public List<StopTimesDTO> findAll() {
        return stopTimesDAO.findAll().stream().sorted(Comparator.comparing(StopTimes::getArrivalTime))
                .map(StopTimesDTO::new)
                .collect(Collectors.toList());
    }

    public List<StopTimesDTO> findByStopId(String stopId) {
        Stops stop = new Stops(stopId);
        return stopTimesDAO.findByStopId(stop).stream().sorted(Comparator.comparing(StopTimes::getArrivalTime))
                .map(StopTimesDTO::new)
                .collect(Collectors.toList());
    }

    public List<StopTimesDTO> findByStopAndRoute(String stopId ,String routeId) {
        return stopTimesDAO.findByStopAndRoute(stopId, routeId).stream().sorted(Comparator.comparing(StopTimes::getArrivalTime)).map(StopTimesDTO::new)
                .collect(Collectors.toList());
    }

    public List<StopTimesDTO> findByStop(String stopId) {
        return stopTimesDAO.findByStop(stopId).stream().sorted(Comparator.comparing(StopTimes::getArrivalTime)).map(StopTimesDTO::new)
                .collect(Collectors.toList());
    }
}
