package es.udc.gtfs.model.exception;

public class FileExistsException extends ModelException {
    public FileExistsException(String fileName) {
        super("El fichero " + fileName + " ya existe");
    }
}
