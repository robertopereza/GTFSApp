package es.udc.gtfs.model.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import es.udc.gtfs.config.Properties;
import es.udc.gtfs.model.domain.OtpProcess;
import es.udc.gtfs.model.domain.Pbf;
import es.udc.gtfs.model.exception.FileExistsException;
import es.udc.gtfs.model.exception.IllegalFileFormatException;
import es.udc.gtfs.model.repository.PbfDao;
import es.udc.gtfs.model.service.dto.PbfDTO;
import es.udc.gtfs.model.service.util.FileValidator;

@Service
@Transactional(readOnly = true, rollbackFor = Exception.class)
public class PbfService {
    @Autowired
    private Properties properties;

    @Autowired
    private PbfDao pbfDAO;

    @Autowired
    private OtpProcess otpProcess;

    public List<PbfDTO> findAll() {
        return pbfDAO.findAll().stream().map(pbf -> new PbfDTO(pbf)).collect(Collectors.toList());
    }

    @Transactional(readOnly = false)
    public void importPbf(MultipartFile file) throws IOException, IllegalFileFormatException, FileExistsException {
        String fileName = file.getOriginalFilename();
        if (!FileValidator.checkPbf(file)) {
            throw new IllegalFileFormatException(fileName, file.getContentType(), ".pbf");
        }
        if (pbfDAO.findByName(fileName) != null) {
            throw new FileExistsException(fileName);
        }
        Pbf pbf = new Pbf();
        pbf.setName(fileName);
        pbfDAO.create(pbf);
        file.transferTo(new File(properties.getRootPath() + "/" + fileName));
    }

    @Transactional(readOnly = false)
    public void buildPbf(String fileName) throws IOException {
        StringBuffer sb = new StringBuffer(fileName);
        sb.deleteCharAt(sb.length() - 1);
        // String path = System.getProperty("java.io.tmpdir");

        deleteBuildedPbf(); // Eliminamos los ficheros pbf del directorio /builded
        copyToBuildedPath(properties.getRootPath(), sb); // Copiamos el fichero a /builded
        runPbf();

    }

    public void runPbf() throws IOException {
        //OtpProcess otpProcess = OtpProcess.getOtpProcessInstance();
        // Comprobamos si hay un proceso en ejecucion de OTP
        if (otpProcess.isActive()) {
            otpProcess.getProcess().destroy();
            otpProcess.setActive(false);
        }
        String cmd = "java -Xmx2G -jar " + properties.getRootPath() + "/otp-1.5.0-shaded.jar --build  " + properties.getBuildedPath()
                + " --inMemory --port 5555";
        Runtime run = Runtime.getRuntime();
        // Ejecutamos el proceso en la instancia
        Process pr = run.exec(cmd);
        otpProcess.setProcess(pr);
        otpProcess.setActive(true);
        BufferedReader buf = new BufferedReader(new InputStreamReader(pr.getInputStream()));
        String line = "";
        while ((line = buf.readLine()) != null) {
            System.out.println(line);
            if (line.contains("Grizzly server running."))
                break;
        }

    }

    public String buildedPbf() {
        File dir = new File(properties.getRootPath());
        File fileList[] = dir.listFiles();
        if (fileList != null)
            for (int i = 0; i < fileList.length; i++) {
                String pes = fileList[i].getName();
                if (pes.endsWith(".pbf")) {
                    //OtpProcess otpProcess = OtpProcess.getOtpProcessInstance();
                    if (otpProcess.isActive()) {
                        return pes;
                    }
                }
            }
        return "";
    }

    @Transactional(readOnly = false)
    public void deletePbf(String fileName) {
        File file = new File(properties.getRootPath() + "/" + fileName);
        file.delete();
        pbfDAO.deletePbf(fileName);
    }

    public void deleteBuildedPbf() {
        File dir = new File(properties.getBuildedPath());
        File fileList[] = dir.listFiles();
        for (int i = 0; i < fileList.length; i++) {
            String pes = fileList[i].getName();
            if (pes.endsWith(".pbf")) {
                fileList[i].delete();
            }
        }
    }

    public void copyToBuildedPath(String path, StringBuffer sb) {
        File source = new File(properties.getRootPath() + "/" + sb);
        File dest = new File(properties.getBuildedPath());
        try {
            FileUtils.copyFileToDirectory(source, dest);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
