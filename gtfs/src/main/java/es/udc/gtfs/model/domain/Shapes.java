package es.udc.gtfs.model.domain;

import java.util.Set;

import javax.annotation.Nullable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.bedatadriven.jackson.datatype.jts.serialization.GeometryDeserializer;
import com.bedatadriven.jackson.datatype.jts.serialization.GeometrySerializer;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.LineString;

@Entity
@Table(name = "shapes_multiline")
public class Shapes {
    @Id
    @Column(name = "shape_id")
    private String id;

    @Column
    @JsonSerialize(using = GeometrySerializer.class)
    @JsonDeserialize(contentUsing = GeometryDeserializer.class)
    private LineString extension;

    @Column(name = "shape_dist_traveled")
    private Double distTraveled;

    @JsonManagedReference
    @OneToMany(mappedBy="shapeId", fetch = FetchType.EAGER, cascade=CascadeType.ALL)
    @Nullable
    private Set<ShapesRoutes> routes;

    public Shapes() {
    }

    public Shapes(String id, Geometry extension, Double distTraveled) {
        this.id = id;
        this.extension = (LineString) extension;
        this.distTraveled = distTraveled;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((distTraveled == null) ? 0 : distTraveled.hashCode());
        result = prime * result + ((extension == null) ? 0 : extension.hashCode());
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Shapes other = (Shapes) obj;
        if (distTraveled == null) {
            if (other.distTraveled != null)
                return false;
        } else if (!distTraveled.equals(other.distTraveled))
            return false;
        if (extension == null) {
            if (other.extension != null)
                return false;
        } else if (!extension.equals(other.extension))
            return false;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public LineString getExtension() {
        return extension;
    }

    public void setExtension(LineString extension) {
        this.extension = extension;
    }

    public Double getDistTraveled() {
        return distTraveled;
    }

    public void setDistTraveled(Double distTraveled) {
        this.distTraveled = distTraveled;
    }

    public Set<ShapesRoutes> getRoutes() {
        return routes;
    }

    public void setRoutes(Set<ShapesRoutes> routes) {
        this.routes = routes;
    }

    
}
