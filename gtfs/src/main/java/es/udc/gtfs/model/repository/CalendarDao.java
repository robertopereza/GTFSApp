package es.udc.gtfs.model.repository;

import java.util.List;

import es.udc.gtfs.model.domain.Calendar;

public interface CalendarDao {
    List<Calendar> findAll();
}
