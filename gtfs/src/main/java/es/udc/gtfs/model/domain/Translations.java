package es.udc.gtfs.model.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;

@Entity
@IdClass(TranslationsCompositeKey.class)
public class Translations {
    @Id
    @Column(name = "table_name")
    private String tableName;

    @Id
    @Column(name = "field_name")
    private String fieldName;

    @Id
    @Column(name = "language")
    private String language;

    @Column(name = "translation")
    private String translation;

    @Column(name = "record_id")
    private String recordId;

    @Column(name = "record_sub_id")
    private String recordSubId;

    @Column(name = "field_value")
    private String fieldValue;

    public Translations() {
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getTranslation() {
        return translation;
    }

    public void setTranslation(String translation) {
        this.translation = translation;
    }

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public String getRecordSubId() {
        return recordSubId;
    }

    public void setRecordSubId(String recordSubId) {
        this.recordSubId = recordSubId;
    }

    public String getFieldValue() {
        return fieldValue;
    }

    public void setFieldValue(String fieldValue) {
        this.fieldValue = fieldValue;
    }




}
