package es.udc.gtfs.model.service.dto;

import java.time.LocalDateTime;

import es.udc.gtfs.model.domain.Calendar;

public class CalendarDTO {
    private String id;

    private Integer monday;

    private Integer tuesday;

    private Integer wednesday;

    private Integer thursday;

    private Integer friday;

    private Integer saturday;

    private Integer sunday;

    private LocalDateTime startDate;

    private LocalDateTime endDate;

    public CalendarDTO() {
    }

    public CalendarDTO(Calendar calendar) {
        this.id = calendar.getId();
        this.monday = calendar.getMonday();
        this.tuesday = calendar.getTuesday();
        this.wednesday = calendar.getWednesday();
        this.thursday = calendar.getThursday();
        this.friday = calendar.getFriday();
        this.saturday = calendar.getSaturday();
        this.sunday = calendar.getSunday();
        this.startDate = calendar.getStartDate();
        this.endDate = calendar.getEndDate();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getMonday() {
        return monday;
    }

    public void setMonday(Integer monday) {
        this.monday = monday;
    }

    public Integer getTuesday() {
        return tuesday;
    }

    public void setTuesday(Integer tuesday) {
        this.tuesday = tuesday;
    }

    public Integer getWednesday() {
        return wednesday;
    }

    public void setWednesday(Integer wednesday) {
        this.wednesday = wednesday;
    }

    public Integer getThursday() {
        return thursday;
    }

    public void setThursday(Integer thursday) {
        this.thursday = thursday;
    }

    public Integer getFriday() {
        return friday;
    }

    public void setFriday(Integer friday) {
        this.friday = friday;
    }

    public Integer getSaturday() {
        return saturday;
    }

    public void setSaturday(Integer saturday) {
        this.saturday = saturday;
    }

    public Integer getSunday() {
        return sunday;
    }

    public void setSunday(Integer sunday) {
        this.sunday = sunday;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
    }

    

    
}
