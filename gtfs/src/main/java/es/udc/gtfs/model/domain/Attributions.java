package es.udc.gtfs.model.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Attributions {
    @Id
    @Column(name = "attribution_id")
    private String id;

    @JoinColumn(name = "agency_id", referencedColumnName = "agency_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private Agency agencyId;

    @JoinColumn(name = "route_id", referencedColumnName = "route_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private Routes routeId;

    @JoinColumn(name = "trip_id", referencedColumnName = "trip_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private Trips tripId;

    @Column(name = "organization_name")
    private String organizationName;

    @Column(name = "is_producer")
    private Boolean isProducer;

    @Column(name = "is_operator")
    private Boolean isOperator;

    @Column(name = "is_authority")
    private Boolean isAuthority;

    @Column(name = "attribution_url")
    private String attributionUrl;

    @Column(name = "attribution_email")
    private String attributionEmail;

    @Column(name = "attribution_phone")
    private String attributionPhone;

    public Attributions() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Agency getAgencyId() {
        return agencyId;
    }

    public void setAgencyId(Agency agencyId) {
        this.agencyId = agencyId;
    }

    public Routes getRouteId() {
        return routeId;
    }

    public void setRouteId(Routes routeId) {
        this.routeId = routeId;
    }

    public Trips getTripId() {
        return tripId;
    }

    public void setTripId(Trips tripId) {
        this.tripId = tripId;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public Boolean getIsProducer() {
        return isProducer;
    }

    public void setIsProducer(Boolean isProducer) {
        this.isProducer = isProducer;
    }

    public Boolean getIsOperator() {
        return isOperator;
    }

    public void setIsOperator(Boolean isOperator) {
        this.isOperator = isOperator;
    }

    public Boolean getIsAuthority() {
        return isAuthority;
    }

    public void setIsAuthority(Boolean isAuthority) {
        this.isAuthority = isAuthority;
    }

    public String getAttributionUrl() {
        return attributionUrl;
    }

    public void setAttributionUrl(String attributionUrl) {
        this.attributionUrl = attributionUrl;
    }

    public String getAttributionEmail() {
        return attributionEmail;
    }

    public void setAttributionEmail(String attributionEmail) {
        this.attributionEmail = attributionEmail;
    }

    public String getAttributionPhone() {
        return attributionPhone;
    }

    public void setAttributionPhone(String attributionPhone) {
        this.attributionPhone = attributionPhone;
    }

    
}
