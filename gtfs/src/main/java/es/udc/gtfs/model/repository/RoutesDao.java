package es.udc.gtfs.model.repository;

import java.util.List;

import es.udc.gtfs.model.domain.Routes;


public interface RoutesDao {
    List<Routes> findAll();

    Routes findById(String id);

    List<Routes> findByStop(String stopId);

}


