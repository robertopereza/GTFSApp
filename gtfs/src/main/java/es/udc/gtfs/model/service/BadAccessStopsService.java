package es.udc.gtfs.model.service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.udc.gtfs.model.domain.BadAccessStops;
import es.udc.gtfs.model.domain.Stops;
import es.udc.gtfs.model.repository.BadAccessStopsDao;
import es.udc.gtfs.model.repository.StopsDao;
import es.udc.gtfs.model.service.dto.BadAccessStopsDTO;


@Service
@Transactional(readOnly = true)
public class BadAccessStopsService {
    @Autowired
    private BadAccessStopsDao badAccessStopsDAO;

    @Autowired
    private StopsDao stopsDAO;

    @Autowired
    private CalculateBadAccessStopsAsync badAccessStopsAsync;

    public List<BadAccessStopsDTO> findAll() {
        return badAccessStopsDAO.findAll().stream().sorted(Comparator.comparing(BadAccessStops::getId)).map(BadAccessStopsDTO::new)
                .collect(Collectors.toList());
    }

    @Transactional(readOnly = false)
    public void calculateBadAccessStops(Integer meters, Integer time) throws ExecutionException, InterruptedException {
        int MIN_SIZE = 600;
        int THREADS = 100;
        List<Stops> stops = stopsDAO.findAll();
        List<List<Stops>> badAccessStops = new ArrayList<>();
        List<CompletableFuture<List<List<Stops>>>> futures = new ArrayList<>();
        List<List<List<Stops>>> results = null;
        System.out.println("Calculando paradas con mala accesibilidad...");
        List<Stops> stopsAux = new ArrayList<>();
        for (Stops stop: stops) {
            if (!CollectionUtils.isEmpty(stop.getStopsRouteType()))
                stopsAux.add(stop);
        }
        stops = stopsAux;
        if (stops.size() > MIN_SIZE) {
            for (int i = 0; i < THREADS; i++) {
                futures.add(badAccessStopsAsync.calculateBadAccessStops(stops, THREADS, i, meters, time));
            }
            results = futures.stream().map(CompletableFuture::join).collect(Collectors.toList());
            for (List<List<Stops>> result: results) 
                for (List<Stops> pair: result)
                    badAccessStops.add(pair);
        } else {
            badAccessStops = futures.stream().map(CompletableFuture::join).collect(Collectors.toList()).get(0);
        }
        System.out.println("Paradas con mala accesibilidad calculadas correctamente.");
        badAccessStopsDAO.deleteAll();
        for (List<Stops> badAccessStop: badAccessStops) {
            BadAccessStops newBadAccessStop = new BadAccessStops();
            newBadAccessStop.setStopId(badAccessStop.get(0));
            newBadAccessStop.setBadAccessStop(badAccessStop.get(1));
            badAccessStopsDAO.create(newBadAccessStop);
        } 
    }
}
