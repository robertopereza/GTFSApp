package es.udc.gtfs.model.repository;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.LineIterator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.support.DataAccessUtils;
import org.springframework.stereotype.Repository;

import es.udc.gtfs.config.Properties;
import es.udc.gtfs.model.domain.Gtfs;
import es.udc.gtfs.model.repository.util.GenericDaoJpa;

@Repository
public class GtfsDaoJpa extends GenericDaoJpa implements GtfsDao {

    @Autowired
    private Properties properties;

    @Override
    public List<Gtfs> findAll() {
        return entityManager.createQuery("from Gtfs", Gtfs.class).getResultList();
    }

    @Override
    public void create(Gtfs gtfs) {
        entityManager.persist(gtfs);
    }

    @Override
    public Gtfs findByName(String name) {
        TypedQuery<Gtfs> query = entityManager.createQuery("from Gtfs g where g.name = :name", Gtfs.class)
                .setParameter("name", name);
        return DataAccessUtils.singleResult(query.getResultList());
    }

    @Override
    public void buildGtfs() throws IOException {
        File scriptGeo = new File(properties.getRootPath() + "/geo_tablas.sql");
        File scriptSql = new File(properties.getRootPath() + "/tablas.sql");

        // Script de creación de tablas stage
        String geo = FileUtils.readFileToString(scriptGeo, "UTF-8");


        // Script de creación de tablas finales
        String sql = FileUtils.readFileToString(scriptSql, "UTF-8");

        File dir = new File(properties.getFilesPath());
        File fileList[] = dir.listFiles();

        Query querySql = entityManager.createNativeQuery(sql);
        querySql.executeUpdate();

        for (File file : fileList) {
            BufferedReader br = new BufferedReader(
                    new InputStreamReader(new FileInputStream(file.getAbsolutePath()), "UTF-8"));
            // Por si hay un caracter especial de UTF-8
            br.mark(1);
            if (br.read() != 0xFEFF)
                br.reset();
            // Las columnas de la tabla que va a insertar el fichero GTFS
            String columns = br.readLine();
            br.close();
            // stg = stg.replaceAll(":" + FilenameUtils.removeExtension(file.getName()) +
            // "columns", columns);
            // stg = stg.replaceAll(":filespath", properties.getFilesPath());
            sql = sql.replaceAll(":" + FilenameUtils.removeExtension(file.getName()) + "columns", columns);

            // Iteramos las filas de los ficheros para insertar los datos en las tablas
            // stage

            LineIterator it = FileUtils.lineIterator(file, "UTF-8");

            String tableName = file.getName().substring(0, file.getName().length() - 4); // Quitar el .txt
            System.out.println("Cargando tabla " + tableName + "...");

            Query insertQuery = null;
            String line = it.nextLine();
            String insert = "INSERT INTO " + tableName
                    + "(" + columns + ")" + " "
                    + "VALUES(";

            while (it.hasNext()) {
                line = it.nextLine();
                line = line.replaceAll("'", "''");
                line = line.replaceAll(",", "','");
                line = "'" + line + "'";
                line = line.replaceAll("''", "null");
                insertQuery = entityManager.createNativeQuery(insert + line + ") ON CONFLICT DO NOTHING");
                //System.out.println(insert + line + ")");
                insertQuery.executeUpdate();
            }


        }
        Query queryStg = entityManager.createNativeQuery(geo);
        queryStg.executeUpdate();

        // Query queryStg = entityManager.createNativeQuery(stg);
        //Query querySql = entityManager.createNativeQuery(sql);
        // queryStg.executeUpdate();
        //System.out.println("Copiando tablas finales...");
        //querySql.executeUpdate();
    }

    @Override
    public void deleteGtfs(String fileName) {
        entityManager.createQuery("DELETE FROM RouteType"
                + " WHERE gtfsId = "
                + " (SELECT id FROM Gtfs g WHERE g.name = :name)")
                .setParameter("name", fileName).executeUpdate();
        entityManager.createQuery("DELETE from Gtfs g where g.name = :name")
                .setParameter("name", fileName).executeUpdate();

    }

    @Override
    public void dropTables() throws IOException {
        File file = new File(properties.getRootPath() + "/drop_tablas.sql");
        String script = FileUtils.readFileToString(file, "UTF-8");
        Query query = entityManager.createNativeQuery(script);
        query.executeUpdate();
    }

}
