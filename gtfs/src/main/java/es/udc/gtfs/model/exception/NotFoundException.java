package es.udc.gtfs.model.exception;

public class NotFoundException extends ModelException {
  public NotFoundException(String id, Class<?> clazz) {
    super("No se encontró " + clazz.getSimpleName() + " con id " + id);
  }
}
