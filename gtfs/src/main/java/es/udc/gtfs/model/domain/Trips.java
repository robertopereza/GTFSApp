package es.udc.gtfs.model.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;



@Entity
public class Trips {
    @JoinColumn(name = "route_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private Routes routeId;

    @JoinColumn(name = "service_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private Calendar serviceId;

    @Id
    @Column(name = "trip_id")
    private String id;

    @Column(name = "trip_headsign")
    private String headsign;

    @Column(name = "trip_short_name")
    private String shortName;

    @Column(name = "direction_id")
    private String directionId;

    @Column(name = "block_id")
    private String blockId;

    @JoinColumn(name = "shape_id")
    @ManyToOne(fetch = FetchType.EAGER)
    //@Transient
    private Shapes shapeId;

    @Column(name = "wheelchair_accessible")
    private String wheelchairAccessible;

    public Trips() {
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((blockId == null) ? 0 : blockId.hashCode());
        result = prime * result + ((directionId == null) ? 0 : directionId.hashCode());
        result = prime * result + ((headsign == null) ? 0 : headsign.hashCode());
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((routeId == null) ? 0 : routeId.hashCode());
        result = prime * result + ((serviceId == null) ? 0 : serviceId.hashCode());
        result = prime * result + ((shapeId == null) ? 0 : shapeId.hashCode());
        result = prime * result + ((shortName == null) ? 0 : shortName.hashCode());
        result = prime * result + ((wheelchairAccessible == null) ? 0 : wheelchairAccessible.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Trips other = (Trips) obj;
        if (blockId == null) {
            if (other.blockId != null)
                return false;
        } else if (!blockId.equals(other.blockId))
            return false;
        if (directionId == null) {
            if (other.directionId != null)
                return false;
        } else if (!directionId.equals(other.directionId))
            return false;
        if (headsign == null) {
            if (other.headsign != null)
                return false;
        } else if (!headsign.equals(other.headsign))
            return false;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (routeId == null) {
            if (other.routeId != null)
                return false;
        } else if (!routeId.equals(other.routeId))
            return false;
        if (serviceId == null) {
            if (other.serviceId != null)
                return false;
        } else if (!serviceId.equals(other.serviceId))
            return false;
        if (shapeId == null) {
            if (other.shapeId != null)
                return false;
        } else if (!shapeId.equals(other.shapeId))
            return false;
        if (shortName == null) {
            if (other.shortName != null)
                return false;
        } else if (!shortName.equals(other.shortName))
            return false;
        if (wheelchairAccessible == null) {
            if (other.wheelchairAccessible != null)
                return false;
        } else if (!wheelchairAccessible.equals(other.wheelchairAccessible))
            return false;
        return true;
    }

    public Routes getRouteId() {
        return routeId;
    }

    public void setRouteId(Routes routeId) {
        this.routeId = routeId;
    }

    public Calendar getServiceId() {
        return serviceId;
    }

    public void setServiceId(Calendar serviceId) {
        this.serviceId = serviceId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHeadsign() {
        return headsign;
    }

    public void setHeadsign(String headsign) {
        this.headsign = headsign;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getDirectionId() {
        return directionId;
    }

    public void setDirectionId(String directionId) {
        this.directionId = directionId;
    }

    public String getBlockId() {
        return blockId;
    }

    public void setBlockId(String blockId) {
        this.blockId = blockId;
    }

    public Shapes getShapeId() {
        return shapeId;
    }

    public void setShapeId(Shapes shapeId) {
        this.shapeId = shapeId;
    }

    public String getWheelchairAccessible() {
        return wheelchairAccessible;
    }

    public void setWheelchairAccessible(String wheelchairAccessible) {
        this.wheelchairAccessible = wheelchairAccessible;
    }

    
}
