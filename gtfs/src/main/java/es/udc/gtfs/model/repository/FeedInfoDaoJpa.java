package es.udc.gtfs.model.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import es.udc.gtfs.model.domain.FeedInfo;
import es.udc.gtfs.model.repository.util.GenericDaoJpa;

@Repository
public class FeedInfoDaoJpa extends GenericDaoJpa implements FeedInfoDao {
    @Override
    public List<FeedInfo> findAll() {
        return entityManager.createQuery("from FeedInfo", FeedInfo.class).getResultList();
    }
}
