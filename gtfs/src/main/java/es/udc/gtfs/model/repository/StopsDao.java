package es.udc.gtfs.model.repository;

import java.util.List;

import es.udc.gtfs.model.domain.Stops;

public interface StopsDao {
    List<Stops> findAll();

    void updateStopsRoutesType();

}
