package es.udc.gtfs.model.service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.udc.gtfs.model.domain.Routes;
import es.udc.gtfs.model.repository.RoutesDao;
import es.udc.gtfs.model.service.dto.RoutesDTO;

@Service
@Transactional(readOnly = true)
public class RoutesService {
    @Autowired
    private RoutesDao routesDAO;

    public List<RoutesDTO> findAll() {
        return routesDAO.findAll().stream().sorted(Comparator.comparing(Routes::getId)).map(RoutesDTO::new)
                .collect(Collectors.toList());
    }

    public List<RoutesDTO> findByStop(String stopId) {
        return routesDAO.findByStop(stopId).stream().sorted(Comparator.comparing(Routes::getId)).map(RoutesDTO::new)
                .collect(Collectors.toList());
    }
}
