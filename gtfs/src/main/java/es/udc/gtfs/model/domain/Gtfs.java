package es.udc.gtfs.model.domain;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "gtfs")
public class Gtfs {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gtfs_generator")
    @SequenceGenerator(name = "gtfs_generator", sequenceName = "gtfs_seq")
    private Long id;

    @Column(unique = true)
    private String name;

    @JsonManagedReference
    @OneToMany(mappedBy="gtfsId", fetch = FetchType.EAGER, cascade=CascadeType.ALL)
    private List<RouteType> routeType;


    public Gtfs() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<RouteType> getRouteType() {
        return routeType;
    }

    public void setRouteType(List<RouteType> routeType) {
        this.routeType = routeType;
    }

}
