package es.udc.gtfs.model.domain;

import javax.annotation.PreDestroy;

import org.springframework.stereotype.Component;

// Singleton para la instancia del proceso de OTP
@Component
public class OtpProcess {
    //private static OtpProcess otpProcess;

    private Process process;

    private boolean active;

    public OtpProcess(){};
    /* 
    private OtpProcess(Process process, boolean active) {
        this.process = process;
        this.active = active;
    }

    public static OtpProcess getOtpProcessInstance() {
        if (otpProcess == null) {
            otpProcess = new OtpProcess(null, false);;
        }
        return otpProcess;
    }
    */
	public Process getProcess() {
		return process;
	}

	public void setProcess(Process process) {
		this.process = process;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

    @PreDestroy
    public void destroy() {
        System.out.println("Destruir proceso");
    }
    

}
