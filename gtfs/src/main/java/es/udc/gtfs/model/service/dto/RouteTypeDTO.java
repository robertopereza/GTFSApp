package es.udc.gtfs.model.service.dto;

import javax.validation.constraints.NotEmpty;

import es.udc.gtfs.model.domain.Gtfs;
import es.udc.gtfs.model.domain.RouteType;

public class RouteTypeDTO {
    private Long id;

    @NotEmpty
    private String routeType;

    @NotEmpty
    private Gtfs gtfsId;

    public RouteTypeDTO() {
    }

    public RouteTypeDTO(RouteType routeType) {
        this.id = routeType.getId();
        this.routeType = routeType.getRouteType();
        this.gtfsId = routeType.getGtfsId();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRouteType() {
        return routeType;
    }

    public void setRouteType(String routeType) {
        this.routeType = routeType;
    }

    public Gtfs getGtfsId() {
        return gtfsId;
    }

    public void setGtfsId(Gtfs gtfsId) {
        this.gtfsId = gtfsId;
    }

    
}
