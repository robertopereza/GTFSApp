package es.udc.gtfs.model.repository;

import java.util.List;

import es.udc.gtfs.model.domain.StopTimes;
import es.udc.gtfs.model.domain.Stops;

public interface StopTimesDao {
    List<StopTimes> findAll();

    List<StopTimes> findByStopId(Stops stopId);

    List<StopTimes> findByStopAndRoute(String stopId, String routeId);

    List<StopTimes> findByStop(String stopId);
}
