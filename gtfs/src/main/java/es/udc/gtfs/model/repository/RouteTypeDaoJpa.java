package es.udc.gtfs.model.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import es.udc.gtfs.model.domain.RouteType;
import es.udc.gtfs.model.repository.util.GenericDaoJpa;

@Repository
public class RouteTypeDaoJpa extends GenericDaoJpa implements RouteTypeDao {

    @Override
    public List<RouteType> findAll() {
        return entityManager.createQuery("from RouteType", RouteType.class).getResultList();
    }
    
}
