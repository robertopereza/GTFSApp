package es.udc.gtfs.model.repository;

import java.io.IOException;
import java.util.List;

import es.udc.gtfs.model.domain.Gtfs;
import es.udc.gtfs.model.domain.RouteType;

public interface StgRoutesDao {
    void loadStageRoutesTable() throws IOException;

    List<RouteType> findStageRouteType(Gtfs gtfs);
}
