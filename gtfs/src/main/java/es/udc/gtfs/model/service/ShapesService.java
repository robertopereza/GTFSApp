package es.udc.gtfs.model.service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.udc.gtfs.model.domain.Shapes;
import es.udc.gtfs.model.repository.ShapesDao;
import es.udc.gtfs.model.service.dto.ShapesDTO;

@Service
@Transactional(readOnly = true)
public class ShapesService {
    @Autowired
    private ShapesDao shapesDAO;

    public List<ShapesDTO> findAll() {
        return shapesDAO.findAll().stream().sorted(Comparator.comparing(Shapes::getId)).map(ShapesDTO::new)
                .collect(Collectors.toList());
    }

    public List<ShapesDTO> findByRoute(String routeId) {
        return shapesDAO.findByRoute(routeId).stream().sorted(Comparator.comparing(Shapes::getId)).map(ShapesDTO::new)
                .collect(Collectors.toList());
    }
}
