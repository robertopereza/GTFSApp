package es.udc.gtfs.model.repository;

import java.util.List;

import javax.persistence.TypedQuery;

import org.springframework.dao.support.DataAccessUtils;
import org.springframework.stereotype.Repository;

import es.udc.gtfs.model.domain.Pbf;
import es.udc.gtfs.model.repository.util.GenericDaoJpa;

@Repository
public class PbfDaoJpa extends GenericDaoJpa implements PbfDao {

    @Override
    public List<Pbf> findAll() {
        return entityManager.createQuery("from Pbf", Pbf.class).getResultList();
    }

    @Override
    public void create(Pbf pbf) {
        entityManager.persist(pbf); 
    }

    @Override
    public Pbf findByName(String name) {
        TypedQuery<Pbf> query = entityManager.createQuery("from Pbf p where p.name = :name", Pbf.class)
          .setParameter("name", name);
        return DataAccessUtils.singleResult(query.getResultList());
    }
    
    @Override
    public void deletePbf(String fileName) {
        entityManager.createQuery("DELETE from Pbf p where p.name = :name")
                .setParameter("name", fileName).executeUpdate();
    }
}
