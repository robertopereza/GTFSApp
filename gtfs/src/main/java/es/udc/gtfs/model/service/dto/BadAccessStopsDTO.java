package es.udc.gtfs.model.service.dto;

import javax.validation.constraints.NotEmpty;

import es.udc.gtfs.model.domain.BadAccessStops;
import es.udc.gtfs.model.domain.Stops;

public class BadAccessStopsDTO {
    private Long id;

    @NotEmpty
    private Stops stopId;

    @NotEmpty
    private Stops badAccessStop;

    public BadAccessStopsDTO() {
    }

    public BadAccessStopsDTO(BadAccessStops badAccessStops) {
        this.id = badAccessStops.getId();
        this.stopId = badAccessStops.getStopId();
        this.badAccessStop = badAccessStops.getBadAccessStop();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Stops getStopId() {
        return stopId;
    }

    public void setStopId(Stops stopId) {
        this.stopId = stopId;
    }

    public Stops getBadAccessStop() {
        return badAccessStop;
    }

    public void setBadAccessStop(Stops badAccessStop) {
        this.badAccessStop = badAccessStop;
    }

    
}
