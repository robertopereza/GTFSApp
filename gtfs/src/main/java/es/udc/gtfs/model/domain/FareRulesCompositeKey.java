package es.udc.gtfs.model.domain;

import java.io.Serializable;

public class FareRulesCompositeKey implements Serializable {
    private FareAttributes fareId;

    private Routes routeId;

    public FareRulesCompositeKey() {
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((fareId == null) ? 0 : fareId.hashCode());
        result = prime * result + ((routeId == null) ? 0 : routeId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        FareRulesCompositeKey other = (FareRulesCompositeKey) obj;
        if (fareId == null) {
            if (other.fareId != null)
                return false;
        } else if (!fareId.equals(other.fareId))
            return false;
        if (routeId == null) {
            if (other.routeId != null)
                return false;
        } else if (!routeId.equals(other.routeId))
            return false;
        return true;
    }

    public FareAttributes getFareId() {
        return fareId;
    }

    public void setFareId(FareAttributes fareId) {
        this.fareId = fareId;
    }

    public Routes getRouteId() {
        return routeId;
    }

    public void setRouteId(Routes routeId) {
        this.routeId = routeId;
    }

    
}
