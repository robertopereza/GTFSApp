package es.udc.gtfs.model.repository;

import java.util.List;

import es.udc.gtfs.model.domain.BadAccessStops;

public interface BadAccessStopsDao {
    List<BadAccessStops> findAll();

    void create(BadAccessStops badAccessStop);

    void deleteAll();
}
