package es.udc.gtfs.model.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@IdClass(TransfersCompositeKey.class)
public class Transfers {
    @Id
    @JoinColumn(name = "from_stop_id", referencedColumnName = "stop_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private Stops fromStopId;

    @Id
    @JoinColumn(name = "to_stop_id", referencedColumnName = "stop_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private Stops toStopId;

    @Id
    @Column(name = "transfer_type")
    private String transferType;

    @Column(name = "min_transfer_time")
    private Integer minTransferTime;

    public Transfers() {
    }

    public Stops getFromStopId() {
        return fromStopId;
    }

    public void setFromStopId(Stops fromStopId) {
        this.fromStopId = fromStopId;
    }

    public Stops getToStopId() {
        return toStopId;
    }

    public void setToStopId(Stops toStopId) {
        this.toStopId = toStopId;
    }

    public String getTransferType() {
        return transferType;
    }

    public void setTransferType(String transferType) {
        this.transferType = transferType;
    }

    public Integer getMinTransferTime() {
        return minTransferTime;
    }

    public void setMinTransferTime(Integer minTransferTime) {
        this.minTransferTime = minTransferTime;
    }

    

}
