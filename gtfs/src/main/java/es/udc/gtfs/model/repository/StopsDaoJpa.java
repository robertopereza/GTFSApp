package es.udc.gtfs.model.repository;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import es.udc.gtfs.model.domain.Stops;
import es.udc.gtfs.model.domain.StopsRouteType;
import es.udc.gtfs.model.repository.util.GenericDaoJpa;
import es.udc.gtfs.model.repository.util.ROUTE_TYPE;

@Repository
public class StopsDaoJpa extends GenericDaoJpa implements StopsDao {
    @Override
    public List<Stops> findAll() {
        return entityManager.createQuery("from Stops", Stops.class).getResultList();
    }

    @Override
    public void updateStopsRoutesType() {
        deleteAllStopsRouteType();
        List<Stops> stops = findAll();
        int n = 0;
        for (Stops stop : stops) {
            Query query = entityManager.createQuery("SELECT DISTINCT(r.type) FROM Routes r"
                    + " JOIN Trips t ON t.routeId = r.id"
                    + " JOIN StopTimes st ON st.tripId = t.id"
                    + " JOIN Stops s ON st.stopId = s.id"
                    + " WHERE s.id = :id"
                    + " GROUP BY r.type").setParameter("id", stop.getId());
            Set<StopsRouteType> set = new HashSet<>();
            for (Object result : query.getResultList()) {
                StopsRouteType stopsRouteType = new StopsRouteType();
                stopsRouteType.setStopId(stop);
                stopsRouteType.setStopsRouteType(ROUTE_TYPE.getValue(Integer.parseInt(result.toString())).name());
                set.add(stopsRouteType);
            }
            if (!set.isEmpty()){
                stop.setStopsRouteType(set);
                entityManager.merge(stop);
                System.out.println("Update de parada " + n);
            }
            n++;
        }
    }

    private void deleteAllStopsRouteType() {
        Query query = entityManager.createQuery("DELETE from StopsRouteType");
        query.executeUpdate();
    }
}
