package es.udc.gtfs.model.repository;

import java.util.List;

import es.udc.gtfs.model.domain.FeedInfo;

public interface FeedInfoDao {
    List<FeedInfo> findAll();
}
