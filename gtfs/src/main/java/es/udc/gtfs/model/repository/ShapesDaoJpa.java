package es.udc.gtfs.model.repository;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import es.udc.gtfs.model.domain.Routes;
import es.udc.gtfs.model.domain.Shapes;
import es.udc.gtfs.model.domain.ShapesRoutes;
import es.udc.gtfs.model.repository.util.GenericDaoJpa;

@Repository
public class ShapesDaoJpa extends GenericDaoJpa implements ShapesDao {
    @Autowired
    RoutesDao routesDao;

    @Override
    public List<Shapes> findAll() {
        return entityManager.createQuery("from Shapes", Shapes.class).getResultList();
    }

    @Override
    public List<Shapes> findByRoute(String routeId) {
        TypedQuery<Shapes> query = entityManager.createQuery("SELECT NEW es.udc.gtfs.model.domain.Shapes(s.id, s.extension, s.distTraveled)  FROM Shapes s"
        + " JOIN Trips t ON s.id = t.shapeId"
        + " JOIN Routes r ON t.routeId = r.id"
        + " WHERE r.id = :routeId"
        + " GROUP BY s.id, s.extension, s.distTraveled", Shapes.class).setParameter("routeId", routeId);

        return query.getResultList();
    }

    @Override
    public void updateShapesRoutes() {
        deleteAllShapesRoutes();
        List<Shapes> shapes = findAll();
        int n = 0;
        for (Shapes shape : shapes) {
            
            Query query = entityManager.createQuery("SELECT r.id FROM Routes r"
                    + " JOIN Trips t ON t.routeId = r.id"
                    + " JOIN Shapes s ON s.id = t.shapeId"
                    + " WHERE s.id = :id").setParameter("id", shape.getId());
            Set<ShapesRoutes> set = new HashSet<>();
            for (Object result : query.getResultList()) {
                ShapesRoutes shapesRoutes = new ShapesRoutes();
                Routes route = routesDao.findById(result.toString());
                shapesRoutes.setRouteId(route);
                shapesRoutes.setShapeId(shape);
                set.add(shapesRoutes);
            }
            if (!set.isEmpty()){
                shape.setRoutes(set);
                entityManager.merge(shape);
                System.out.println("Update de shape " + n);
            }
            n++;
        }
        
    }

    private void deleteAllShapesRoutes() {
        Query query = entityManager.createQuery("DELETE from ShapesRoutes");
        query.executeUpdate();
    }
}
