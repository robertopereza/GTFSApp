package es.udc.gtfs.model.repository.util;

public enum ROUTE_TYPE {
    TRANVÍA(0),
    METRO(1),
    TREN(2),
    AUTOBÚS(3),
    TRANSBORDADOR(4),
    TRANVÍA_DE_CABLE(5),
    TELEFÉRICO(6),
    FUNICULAR(7),
    TROLEBÚS(11),
    MONORRIEL(12);


    private int numVal;

    ROUTE_TYPE(int numVal) {
        this.numVal = numVal;
    }

    public int getNumVal() {
        return numVal;
    }

    public static ROUTE_TYPE getValue(int value) {
        for (ROUTE_TYPE rType: ROUTE_TYPE.values()) {
            if (rType.numVal == value) return rType;
        }
        throw new IllegalArgumentException("Value not found");
     }
}
