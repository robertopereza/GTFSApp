package es.udc.gtfs.model.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import es.udc.gtfs.model.domain.Routes;
import es.udc.gtfs.model.domain.Trips;
import es.udc.gtfs.model.repository.util.GenericDaoJpa;

@Repository
public class TripsDaoJpa extends GenericDaoJpa implements TripsDao {
    @Override
    public List<Trips> findAll() {
        return entityManager.createQuery("from Trips", Trips.class).getResultList();
    }

    @Override
    public List<Trips> findByRouteId(Routes routeId) {
        return entityManager.createQuery("from Trips where routeId = :routeId", Trips.class).setParameter("routeId", routeId).getResultList();
    }
}
