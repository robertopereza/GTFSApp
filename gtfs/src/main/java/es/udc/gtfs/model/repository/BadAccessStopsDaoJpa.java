package es.udc.gtfs.model.repository;

import java.util.List;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import es.udc.gtfs.model.domain.BadAccessStops;
import es.udc.gtfs.model.repository.util.GenericDaoJpa;

@Repository
public class BadAccessStopsDaoJpa extends GenericDaoJpa implements BadAccessStopsDao {

    @Override
    public List<BadAccessStops> findAll() {
        return entityManager.createQuery("from BadAccessStops", BadAccessStops.class).getResultList();
    }

    @Override
    public void create(BadAccessStops badAccessStop) {
        entityManager.persist(badAccessStop);
    }

    @Override
    public void deleteAll() {
        Query query = entityManager.createQuery("DELETE from BadAccessStops");
        query.executeUpdate();
    }
    
}
