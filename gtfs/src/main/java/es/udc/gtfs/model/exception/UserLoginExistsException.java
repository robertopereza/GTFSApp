package es.udc.gtfs.model.exception;

public class UserLoginExistsException extends ModelException {
    public UserLoginExistsException(String login) {
      super("El login de usuario " + login + " ya existe");
    }
  }
  