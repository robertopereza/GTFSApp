package es.udc.gtfs.model.repository;

import java.util.List;

import es.udc.gtfs.model.domain.RouteType;

public interface RouteTypeDao {
    List<RouteType> findAll();
}
