package es.udc.gtfs.model.exception;

public class IllegalFileFormatException extends ModelException {

    public IllegalFileFormatException(String fileName, String wrongFormat, String rightFormat) {
        super("El formato del fichero " + fileName + " es " + wrongFormat + " y debe ser " + rightFormat);
    }
    
}
