package es.udc.gtfs.model.service.dto;

import java.util.Set;

import org.locationtech.jts.geom.LineString;

import es.udc.gtfs.model.domain.Shapes;
import es.udc.gtfs.model.domain.ShapesRoutes;

public class ShapesDTO {
    private String id;
    
    private LineString extension;

    private Double distTraveled;

    private Set<ShapesRoutes> routes;

    public ShapesDTO() {
    }

    public ShapesDTO(Shapes shapes) {
        this.id = shapes.getId();
        this.extension = shapes.getExtension();
        this.distTraveled = shapes.getDistTraveled();
        this.routes = shapes.getRoutes();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public LineString getExtension() {
        return extension;
    }

    public void setExtension(LineString extension) {
        this.extension = extension;
    }

    public Double getDistTraveled() {
        return distTraveled;
    }

    public void setDistTraveled(Double distTraveled) {
        this.distTraveled = distTraveled;
    }

    public Set<ShapesRoutes> getRoutes() {
        return routes;
    }

    public void setRoutes(Set<ShapesRoutes> routes) {
        this.routes = routes;
    }

    
}
