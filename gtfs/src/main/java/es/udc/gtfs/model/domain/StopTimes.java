package es.udc.gtfs.model.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;


@Entity
@IdClass(StopTimesCompositeKey.class)
public class StopTimes {
    @Id
    @JoinColumn(name = "trip_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private Trips tripId;

    @Id
    @Column(name = "arrival_time")
    private String arrivalTime;

    @Column(name = "departure_time")
    private String departureTime;

    @Id
    @JoinColumn(name = "stop_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private Stops stopId;

    @Column(name = "stop_sequence")
    private String sequence;

    @Column(name = "stop_headsign")
    private String headsign;

    @Column(name = "pickup_type")
    private String pickupType;

    @Column(name = "drop_off_type")
    private String dropOffType;

    @Column(name = "shape_dist_traveled")
    private Float shapeDistTraveled;

    public StopTimes() {
    }

    public StopTimes(Trips tripId, String arrivalTime, String departureTime, Stops stopId, String sequence,
            String headsign, String pickupType, String dropOffType, Float shapeDistTraveled) {
        this.tripId = tripId;
        this.arrivalTime = arrivalTime;
        this.departureTime = departureTime;
        this.stopId = stopId;
        this.sequence = sequence;
        this.headsign = headsign;
        this.pickupType = pickupType;
        this.dropOffType = dropOffType;
        this.shapeDistTraveled = shapeDistTraveled;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((arrivalTime == null) ? 0 : arrivalTime.hashCode());
        result = prime * result + ((departureTime == null) ? 0 : departureTime.hashCode());
        result = prime * result + ((dropOffType == null) ? 0 : dropOffType.hashCode());
        result = prime * result + ((headsign == null) ? 0 : headsign.hashCode());
        result = prime * result + ((pickupType == null) ? 0 : pickupType.hashCode());
        result = prime * result + ((sequence == null) ? 0 : sequence.hashCode());
        result = prime * result + ((shapeDistTraveled == null) ? 0 : shapeDistTraveled.hashCode());
        result = prime * result + ((stopId == null) ? 0 : stopId.hashCode());
        result = prime * result + ((tripId == null) ? 0 : tripId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        StopTimes other = (StopTimes) obj;
        if (arrivalTime == null) {
            if (other.arrivalTime != null)
                return false;
        } else if (!arrivalTime.equals(other.arrivalTime))
            return false;
        if (departureTime == null) {
            if (other.departureTime != null)
                return false;
        } else if (!departureTime.equals(other.departureTime))
            return false;
        if (dropOffType == null) {
            if (other.dropOffType != null)
                return false;
        } else if (!dropOffType.equals(other.dropOffType))
            return false;
        if (headsign == null) {
            if (other.headsign != null)
                return false;
        } else if (!headsign.equals(other.headsign))
            return false;
        if (pickupType == null) {
            if (other.pickupType != null)
                return false;
        } else if (!pickupType.equals(other.pickupType))
            return false;
        if (sequence == null) {
            if (other.sequence != null)
                return false;
        } else if (!sequence.equals(other.sequence))
            return false;
        if (shapeDistTraveled == null) {
            if (other.shapeDistTraveled != null)
                return false;
        } else if (!shapeDistTraveled.equals(other.shapeDistTraveled))
            return false;
        if (stopId == null) {
            if (other.stopId != null)
                return false;
        } else if (!stopId.equals(other.stopId))
            return false;
        if (tripId == null) {
            if (other.tripId != null)
                return false;
        } else if (!tripId.equals(other.tripId))
            return false;
        return true;
    }

    public Trips getTripId() {
        return tripId;
    }

    public void setTripId(Trips tripId) {
        this.tripId = tripId;
    }

    public String getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(String arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public String getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(String departureTime) {
        this.departureTime = departureTime;
    }

    public Stops getStopId() {
        return stopId;
    }

    public void setStopId(Stops stopId) {
        this.stopId = stopId;
    }

    public String getSequence() {
        return sequence;
    }

    public void setSequence(String sequence) {
        this.sequence = sequence;
    }

    public String getHeadsign() {
        return headsign;
    }

    public void setHeadsign(String headsign) {
        this.headsign = headsign;
    }

    
    public String getPickupType() {
        return pickupType;
    }



    public void setPickupType(String pickupType) {
        this.pickupType = pickupType;
    }



    public String getDropOffType() {
        return dropOffType;
    }

    public void setDropOffType(String dropOffType) {
        this.dropOffType = dropOffType;
    }

    public Float getShapeDistTraveled() {
        return shapeDistTraveled;
    }

    public void setShapeDistTraveled(Float shapeDistTraveled) {
        this.shapeDistTraveled = shapeDistTraveled;
    }

    
}
