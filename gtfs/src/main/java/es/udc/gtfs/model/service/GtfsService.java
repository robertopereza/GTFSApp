package es.udc.gtfs.model.service;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import es.udc.gtfs.config.Properties;
import es.udc.gtfs.model.domain.Gtfs;
import es.udc.gtfs.model.domain.OtpProcess;
import es.udc.gtfs.model.domain.RouteType;
import es.udc.gtfs.model.exception.FileExistsException;
import es.udc.gtfs.model.exception.IllegalFileFormatException;
import es.udc.gtfs.model.repository.BadAccessStopsDao;
import es.udc.gtfs.model.repository.GtfsDao;
import es.udc.gtfs.model.repository.ShapesDao;
import es.udc.gtfs.model.repository.StgRoutesDao;
import es.udc.gtfs.model.repository.StopsDao;
import es.udc.gtfs.model.service.dto.GtfsDTO;
import es.udc.gtfs.model.service.util.FileValidator;
import net.lingala.zip4j.ZipFile;
import net.lingala.zip4j.exception.ZipException;

@Service
@Transactional(readOnly = true, rollbackFor = Exception.class)
public class GtfsService {
    @Autowired
    private Properties properties;

    @Autowired
    private GtfsDao gtfsDAO;

    @Autowired
    private StopsDao stopsDAO;

    @Autowired
    private ShapesDao shapesDAO;

    @Autowired
    private StgRoutesDao stgRoutesDao;

    @Autowired
    private OtpProcess otpProcess;

    @Autowired
    private BadAccessStopsDao badAccessStopsDao;
    
    public List<GtfsDTO> findAll() {
        return gtfsDAO.findAll().stream().map(gtfs -> new GtfsDTO(gtfs)).collect(Collectors.toList());
    }
    
    @Transactional(readOnly = false)
    public void importGtfs(MultipartFile file) throws IOException, IllegalFileFormatException, FileExistsException {
        String fileName = file.getOriginalFilename();
        if (!FileValidator.checkZip(file)) {
            throw new IllegalFileFormatException(fileName, file.getContentType(), ".zip");
        }
        if (gtfsDAO.findByName(fileName) != null) {
            throw new FileExistsException(fileName);
        }
        file.transferTo(new File(properties.getRootPath() + "/" + fileName));
        unzip(properties.getRootPath() + "/" + fileName, properties.getFilesPath());
        stgRoutesDao.loadStageRoutesTable();
        Gtfs gtfs = new Gtfs();
        List<RouteType> routeTypes = stgRoutesDao.findStageRouteType(gtfs);
        gtfs.setName(fileName);
        gtfs.setRouteType(routeTypes);
        gtfsDAO.create(gtfs);
        
    }

    @Transactional(readOnly = false)
    public void buildGtfs(List<String> files) throws IOException {
        if (otpProcess.isActive()) {
            otpProcess.getProcess().destroy();
            otpProcess.setActive(false);
        }
        deleteBuildedZip(); // Eliminamos los ficheros zip del directorio /builded
        gtfsDAO.dropTables();
        for (String file: files) {
            StringBuffer sb = new StringBuffer(file);
            //sb.deleteCharAt(sb.length() - 1);
            System.out.println("Cargando fichero " + sb);
            copyToBuildedPath(properties.getRootPath(), sb); // Copiamos el fichero a /builded
            String buildedFilePath = properties.getBuildedPath() + "/" + sb;
            unzip(buildedFilePath, properties.getFilesPath());
            gtfsDAO.buildGtfs();
        }
        stopsDAO.updateStopsRoutesType();
        shapesDAO.updateShapesRoutes();
        badAccessStopsDao.deleteAll();
    }

    public List<String> buildedGtfs() {
        File dir = new File(properties.getBuildedPath());
        File fileList[] = dir.listFiles();
        List<String> files = new ArrayList<>();
        for (int i = 0; i < fileList.length; i++) {
            String fileName = fileList[i].getName();
            if (fileName.endsWith(".zip")) {
                files.add(fileName);
            }
        }

        return files;
    }

    @Transactional(readOnly = false)
    public void deleteGtfs(String fileName) {
        File file = new File(properties.getRootPath() + "/" + fileName);
        file.delete();
        gtfsDAO.deleteGtfs(fileName);
    }

    public void deleteBuildedZip() {
        File dir = new File(properties.getBuildedPath());
        File[] fileList = dir.listFiles();

        for (int i = 0; i < fileList.length; i++) {
            String pes = fileList[i].getName();
            if (pes.endsWith(".zip")) {
                fileList[i].delete();
            }
        }

    }

    public void copyToBuildedPath(String path, StringBuffer sb) {
        File source = new File(properties.getRootPath() + "/" + sb);
        File dest = new File(properties.getBuildedPath());
        try {
            FileUtils.copyFileToDirectory(source, dest);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void unzip(String source, String destination) throws IOException {
        try {
            ZipFile zipFile = new ZipFile(source);
            zipFile.extractAll(destination);
            zipFile.close();
        } catch (ZipException e) {
            e.printStackTrace();
        }
    }
}
