package es.udc.gtfs.model.repository;

import java.io.IOException;
import java.util.List;

import es.udc.gtfs.model.domain.Gtfs;

public interface GtfsDao {
    List<Gtfs> findAll();

    Gtfs findByName(String name);

    void create(Gtfs gtfs);

    void buildGtfs() throws IOException;

    void deleteGtfs(String fileName);

    void dropTables() throws IOException;
}
