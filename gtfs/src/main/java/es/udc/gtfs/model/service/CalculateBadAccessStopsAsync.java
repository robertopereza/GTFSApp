package es.udc.gtfs.model.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.locationtech.jts.operation.distance.DistanceOp;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import es.udc.gtfs.config.Properties;
import es.udc.gtfs.model.domain.Stops;


@Service
public class CalculateBadAccessStopsAsync {

    @Autowired
    private Properties properties;

    public CalculateBadAccessStopsAsync() {
    }

    @Async
    public CompletableFuture<List<List<Stops>>> calculateBadAccessStops(List<Stops> stops, int threads, int threadNumber, Integer meters, Integer time) throws InterruptedException {
        int DISTANCEBETWEENSTOPS = meters;
        int MINUTES = time;
        List<List<Stops>> badAccessStops = new ArrayList<>();
        RestTemplate restTemplate = new RestTemplate();
        Map<String, String> uriParams = new HashMap<String, String>();
        ResponseEntity<String> response = null;
        ObjectMapper mapper = new ObjectMapper();
        Integer duration = 0;
        Integer totalIterations = 0;
        List<Stops> stopsAux = new ArrayList<>();
        for (int j = threadNumber; j < stops.size(); j += threads) {
            stopsAux.add(stops.get(j));  
        }
        for (int i = 0; i < stopsAux.size(); i++) {
            for (int j = 0; j < stops.size(); j++) {
                if (stopsAux.get(i) != stops.get(j)) {
                    double distance = DistanceOp.distance(stopsAux.get(i).getPoint(), stops.get(j).getPoint()) * 100000;
                    if (distance <= DISTANCEBETWEENSTOPS) {
                        uriParams.put("fromPlace", stopsAux.get(i).getPoint().toText());
                        uriParams.put("toPlace", stops.get(j).getPoint().toText());
                        uriParams.put("mode", "WALK, BUS, SUBWAY");
                        uriParams.put("numItineraries", "1");
                        response = restTemplate.getForEntity(properties.getPlannerUri(), String.class, uriParams);
                        try {
                            JsonNode node = mapper.readTree(response.getBody());
                            if (node.get("plan") != null) { // Puede devolver ningún plan
                                duration = node.get("plan").get("itineraries").get(0).get("duration").asInt();
                            }
                        } catch (JsonProcessingException e) {
                            e.printStackTrace();
                        }
                        if (duration > 60 * MINUTES) {
                            badAccessStops.add(Arrays.asList(stopsAux.get(i), stops.get(j)));
                        }
                    }
                    totalIterations++;
                }
            }
            
        }
        System.out.println("Thread " + threadNumber + " ha calculado " + totalIterations + " pares de paradas.");
        return CompletableFuture.completedFuture(badAccessStops);
    }
}
