package es.udc.gtfs.model.repository;

import java.util.List;

import es.udc.gtfs.model.domain.Agency;

public interface AgencyDao {
    List<Agency> findAll();
}
