package es.udc.gtfs.model.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Pathways {
    @Id
    @Column(name = "pathway_id")
    private String id;

    @JoinColumn(name = "from_stop_id", referencedColumnName = "stop_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private Stops fromStopId;

    @JoinColumn(name = "to_stop_id", referencedColumnName = "stop_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private Stops toStopId;

    @Column(name = "pathway_mode")
    private String pathwayMode;

    @Column(name = "is_bidirectional")
    private Boolean isBidirectional;

    @Column
    private Float length;

    @Column(name = "traversal_time")
    private Integer traversalTime;

    @Column(name = "stair_count")
    private Integer stairCount;

    @Column(name = "max_slope")
    private Float maxSlope;

    @Column(name = "min_width")
    private Float minWidth;

    @Column(name = "signposted_as")
    private String signpostedAs;

    @Column(name = "reversed_signposted_as")
    private String reversedSignpostedAs;

    public Pathways() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Stops getFromStopId() {
        return fromStopId;
    }

    public void setFromStopId(Stops fromStopId) {
        this.fromStopId = fromStopId;
    }

    public Stops getToStopId() {
        return toStopId;
    }

    public void setToStopId(Stops toStopId) {
        this.toStopId = toStopId;
    }

    public String getPathwayMode() {
        return pathwayMode;
    }

    public void setPathwayMode(String pathwayMode) {
        this.pathwayMode = pathwayMode;
    }

    public Boolean getIsBidirectional() {
        return isBidirectional;
    }

    public void setIsBidirectional(Boolean isBidirectional) {
        this.isBidirectional = isBidirectional;
    }

    public Float getLength() {
        return length;
    }

    public void setLength(Float length) {
        this.length = length;
    }

    public Integer getTraversalTime() {
        return traversalTime;
    }

    public void setTraversalTime(Integer traversalTime) {
        this.traversalTime = traversalTime;
    }

    public Integer getStairCount() {
        return stairCount;
    }

    public void setStairCount(Integer stairCount) {
        this.stairCount = stairCount;
    }

    public Float getMaxSlope() {
        return maxSlope;
    }

    public void setMaxSlope(Float maxSlope) {
        this.maxSlope = maxSlope;
    }

    public Float getMinWidth() {
        return minWidth;
    }

    public void setMinWidth(Float minWidth) {
        this.minWidth = minWidth;
    }

    public String getSignpostedAs() {
        return signpostedAs;
    }

    public void setSignpostedAs(String signpostedAs) {
        this.signpostedAs = signpostedAs;
    }

    public String getReversedSignpostedAs() {
        return reversedSignpostedAs;
    }

    public void setReversedSignpostedAs(String reversedSignpostedAs) {
        this.reversedSignpostedAs = reversedSignpostedAs;
    }

}
