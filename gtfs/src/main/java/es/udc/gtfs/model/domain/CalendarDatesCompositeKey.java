package es.udc.gtfs.model.domain;

import java.io.Serializable;
import java.time.LocalDateTime;

public class CalendarDatesCompositeKey implements Serializable {
    private Calendar id;
    
    private LocalDateTime date;

    public CalendarDatesCompositeKey() {
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((date == null) ? 0 : date.hashCode());
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        CalendarDatesCompositeKey other = (CalendarDatesCompositeKey) obj;
        if (date == null) {
            if (other.date != null)
                return false;
        } else if (!date.equals(other.date))
            return false;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }

    public Calendar getId() {
        return id;
    }

    public void setId(Calendar id) {
        this.id = id;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    
}
