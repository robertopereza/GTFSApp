package es.udc.gtfs.model.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import es.udc.gtfs.model.domain.Calendar;
import es.udc.gtfs.model.repository.util.GenericDaoJpa;

@Repository
public class CalendarDaoJpa extends GenericDaoJpa implements CalendarDao {
    @Override
    public List<Calendar> findAll() {
        return entityManager.createQuery("from Calendar", Calendar.class).getResultList();
    }
}
