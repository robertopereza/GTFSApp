package es.udc.gtfs.model.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "pbf")
public class Pbf {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pbf_generator")
    @SequenceGenerator(name = "pbf_generator", sequenceName = "pbf_seq")
    @Column(name = "pbf_id")
    private Long id;

    @Column(unique = true)
    private String name;

    public Pbf() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

   

    
}
