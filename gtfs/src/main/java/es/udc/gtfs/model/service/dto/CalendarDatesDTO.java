package es.udc.gtfs.model.service.dto;

import java.time.LocalDateTime;

import es.udc.gtfs.model.domain.Calendar;
import es.udc.gtfs.model.domain.CalendarDates;

public class CalendarDatesDTO {
    private Calendar id;

    private LocalDateTime date;

    private Integer exceptionType;

    public CalendarDatesDTO() {
    }

    public CalendarDatesDTO(CalendarDates calendarDates) {
        this.id = calendarDates.getId();
        this.date = calendarDates.getDate();
        this.exceptionType = calendarDates.getExceptionType();
    }

    public Calendar getId() {
        return id;
    }

    public void setId(Calendar id) {
        this.id = id;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public Integer getExceptionType() {
        return exceptionType;
    }

    public void setExceptionType(Integer exceptionType) {
        this.exceptionType = exceptionType;
    }

    
}
