package es.udc.gtfs.model.repository;

import java.util.List;

import es.udc.gtfs.model.domain.Pbf;

public interface PbfDao {
    List<Pbf> findAll();

    Pbf findByName(String name);

    void create(Pbf pbf);

    void deletePbf(String fileName);
}
