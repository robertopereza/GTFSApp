package es.udc.gtfs.model.service.util;

import java.io.IOException;

import org.springframework.web.multipart.MultipartFile;

public class FileValidator {

    public static boolean checkPbf(MultipartFile file) throws IOException {
        String fileName = file.getOriginalFilename().toUpperCase();
        boolean extension = fileName.endsWith(".OSM.PBF");
        if (!extension) {
           return false;
        }
        return true;
    }

    public static boolean checkZip(MultipartFile file) throws IOException {
        String fileName = file.getOriginalFilename().toUpperCase();
        boolean extension = fileName.endsWith(".ZIP");
        if (!extension) {
           return false;
        }
        return true;
    }
}
