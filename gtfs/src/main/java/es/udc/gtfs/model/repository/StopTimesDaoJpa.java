package es.udc.gtfs.model.repository;

import java.util.List;
import java.util.Locale;

import javax.persistence.TypedQuery;

import org.joda.time.DateTime;
import org.springframework.stereotype.Repository;

import es.udc.gtfs.model.domain.StopTimes;
import es.udc.gtfs.model.domain.Stops;
import es.udc.gtfs.model.repository.util.GenericDaoJpa;

@Repository
public class StopTimesDaoJpa extends GenericDaoJpa implements StopTimesDao {
    @Override
    public List<StopTimes> findAll() {
        return entityManager.createQuery("from StopTimes", StopTimes.class).getResultList();
    }

    @Override
    public List<StopTimes> findByStopId(Stops stopId) {
        return entityManager.createQuery("from StopTimes where stopId = :stopId", StopTimes.class).setParameter("stopId", stopId).getResultList();
    }

    @Override
    public List<StopTimes> findByStopAndRoute(String stopId, String routeId) {
        TypedQuery<StopTimes> query = entityManager.createQuery("SELECT NEW es.udc.gtfs.model.domain.StopTimes(st.tripId, st.arrivalTime, st.departureTime, st.stopId, st.sequence, st.headsign, st.pickupType, st.dropOffType, st.shapeDistTraveled)  FROM StopTimes st"
        + " JOIN Trips t ON st.tripId = t.id"
        + " JOIN Calendar c ON t.serviceId = c.id"
        + " JOIN Routes r ON t.routeId = r.id"
        + " JOIN Stops s ON st.stopId = s.id"
        + " WHERE s.id = :stopId"
        + " AND r.id = :routeId"
        + " AND current_date between c.startDate AND c.endDate"
        + " AND c." + DateTime.now().dayOfWeek().getAsText(Locale.US).toLowerCase() + " = 1"
        + " GROUP BY st.tripId, st.arrivalTime, st.departureTime, st.stopId, st.sequence, st.headsign, st.dropOffType, st.shapeDistTraveled"
        + " ORDER BY st.arrivalTime", StopTimes.class).setParameter("stopId", stopId).setParameter("routeId", routeId);

        return query.getResultList();
    }

    @Override
    public List<StopTimes> findByStop(String stopId) {
        TypedQuery<StopTimes> query = entityManager.createQuery("SELECT NEW es.udc.gtfs.model.domain.StopTimes(st.tripId, st.arrivalTime, st.departureTime, st.stopId, st.sequence, st.headsign, st.pickupType, st.dropOffType, st.shapeDistTraveled)  FROM StopTimes st"
        + " JOIN Trips t ON st.tripId = t.id"
        + " JOIN Calendar c ON t.serviceId = c.id"
        + " JOIN Routes r ON t.routeId = r.id"
        + " JOIN Stops s ON st.stopId = s.id"
        + " WHERE s.id = :stopId"
        + " AND current_date between c.startDate AND c.endDate"
        + " AND c." + DateTime.now().dayOfWeek().getAsText(Locale.US).toLowerCase() + " = 1"
        + " GROUP BY st.tripId, st.arrivalTime, st.departureTime, st.stopId, st.sequence, st.headsign, st.pickupType, st.dropOffType, st.shapeDistTraveled"
        + " ORDER BY st.arrivalTime", StopTimes.class).setParameter("stopId", stopId);
        return query.getResultList();
    }
}
