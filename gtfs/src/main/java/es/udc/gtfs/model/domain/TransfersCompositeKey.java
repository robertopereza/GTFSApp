package es.udc.gtfs.model.domain;

import java.io.Serializable;

public class TransfersCompositeKey implements Serializable {
    private Stops fromStopId;

    private Stops toStopId;

    private String transferType;

    public TransfersCompositeKey() {
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((fromStopId == null) ? 0 : fromStopId.hashCode());
        result = prime * result + ((toStopId == null) ? 0 : toStopId.hashCode());
        result = prime * result + ((transferType == null) ? 0 : transferType.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        TransfersCompositeKey other = (TransfersCompositeKey) obj;
        if (fromStopId == null) {
            if (other.fromStopId != null)
                return false;
        } else if (!fromStopId.equals(other.fromStopId))
            return false;
        if (toStopId == null) {
            if (other.toStopId != null)
                return false;
        } else if (!toStopId.equals(other.toStopId))
            return false;
        if (transferType == null) {
            if (other.transferType != null)
                return false;
        } else if (!transferType.equals(other.transferType))
            return false;
        return true;
    }

    public Stops getFromStopId() {
        return fromStopId;
    }

    public void setFromStopId(Stops fromStopId) {
        this.fromStopId = fromStopId;
    }

    public Stops getToStopId() {
        return toStopId;
    }

    public void setToStopId(Stops toStopId) {
        this.toStopId = toStopId;
    }

    public String getTransferType() {
        return transferType;
    }

    public void setTransferType(String transferType) {
        this.transferType = transferType;
    }

    
}
