package es.udc.gtfs.model.domain;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "calendar")
public class Calendar {
    @Id
    @Column(name = "service_id")
    private String id;

    @Column
    private Integer monday;

    @Column
    private Integer tuesday;

    @Column
    private Integer wednesday;

    @Column
    private Integer thursday;

    @Column
    private Integer friday;

    @Column
    private Integer saturday;

    @Column
    private Integer sunday;

    @Column(name = "start_date")
    private LocalDateTime startDate;

    @Column(name = "end_date")
    private LocalDateTime endDate;

    public Calendar() {
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((endDate == null) ? 0 : endDate.hashCode());
        result = prime * result + ((friday == null) ? 0 : friday.hashCode());
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((monday == null) ? 0 : monday.hashCode());
        result = prime * result + ((saturday == null) ? 0 : saturday.hashCode());
        result = prime * result + ((startDate == null) ? 0 : startDate.hashCode());
        result = prime * result + ((sunday == null) ? 0 : sunday.hashCode());
        result = prime * result + ((thursday == null) ? 0 : thursday.hashCode());
        result = prime * result + ((tuesday == null) ? 0 : tuesday.hashCode());
        result = prime * result + ((wednesday == null) ? 0 : wednesday.hashCode());
        return result;
    }



    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Calendar other = (Calendar) obj;
        if (endDate == null) {
            if (other.endDate != null)
                return false;
        } else if (!endDate.equals(other.endDate))
            return false;
        if (friday == null) {
            if (other.friday != null)
                return false;
        } else if (!friday.equals(other.friday))
            return false;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (monday == null) {
            if (other.monday != null)
                return false;
        } else if (!monday.equals(other.monday))
            return false;
        if (saturday == null) {
            if (other.saturday != null)
                return false;
        } else if (!saturday.equals(other.saturday))
            return false;
        if (startDate == null) {
            if (other.startDate != null)
                return false;
        } else if (!startDate.equals(other.startDate))
            return false;
        if (sunday == null) {
            if (other.sunday != null)
                return false;
        } else if (!sunday.equals(other.sunday))
            return false;
        if (thursday == null) {
            if (other.thursday != null)
                return false;
        } else if (!thursday.equals(other.thursday))
            return false;
        if (tuesday == null) {
            if (other.tuesday != null)
                return false;
        } else if (!tuesday.equals(other.tuesday))
            return false;
        if (wednesday == null) {
            if (other.wednesday != null)
                return false;
        } else if (!wednesday.equals(other.wednesday))
            return false;
        return true;
    }



    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getMonday() {
        return monday;
    }

    public void setMonday(Integer monday) {
        this.monday = monday;
    }

    public Integer getTuesday() {
        return tuesday;
    }

    public void setTuesday(Integer tuesday) {
        this.tuesday = tuesday;
    }

    public Integer getWednesday() {
        return wednesday;
    }

    public void setWednesday(Integer wednesday) {
        this.wednesday = wednesday;
    }

    public Integer getThursday() {
        return thursday;
    }

    public void setThursday(Integer thursday) {
        this.thursday = thursday;
    }

    public Integer getFriday() {
        return friday;
    }

    public void setFriday(Integer friday) {
        this.friday = friday;
    }

    public Integer getSaturday() {
        return saturday;
    }

    public void setSaturday(Integer saturday) {
        this.saturday = saturday;
    }

    public Integer getSunday() {
        return sunday;
    }

    public void setSunday(Integer sunday) {
        this.sunday = sunday;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
    }

    
}
