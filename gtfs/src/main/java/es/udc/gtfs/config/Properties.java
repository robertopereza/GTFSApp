package es.udc.gtfs.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "properties")
public class Properties {
  // Necesario para usarlo en JWTFilter
  public static String CLIENT_HOST;

  private String clientHost;
  private String jwtSecretKey;
  private Long jwtValidity;
  private String imagesPath;
  private String rootPath;
  private String buildedPath;  
  private String filesPath;
  private String plannerUri;
  private String isochroneUri;

  public String getClientHost() {
    return clientHost;
  }

  public void setClientHost(String clientHost) {
    Properties.CLIENT_HOST = clientHost;
    this.clientHost = clientHost;
  }

  public String getJwtSecretKey() {
    return jwtSecretKey;
  }

  public void setJwtSecretKey(String jwtSecretKey) {
    this.jwtSecretKey = jwtSecretKey;
  }

  public Long getJwtValidity() {
    return jwtValidity;
  }

  public void setJwtValidity(Long jwtValidity) {
    this.jwtValidity = jwtValidity;
  }

  public String getImagesPath() {
    return imagesPath;
  }

  public void setImagesPath(String imagesPath) {
    this.imagesPath = imagesPath;
  }

  public String getRootPath() {
    return rootPath;
  }

  public void setRootPath(String rootPath) {
    this.rootPath = rootPath;
  }

  public String getBuildedPath() {
    return buildedPath;
  }

  public void setBuildedPath(String buildedPath) {
    this.buildedPath = buildedPath;
  }

  public String getFilesPath() {
    return filesPath;
  }

  public void setFilesPath(String filesPath) {
    this.filesPath = filesPath;
  }

  public String getPlannerUri() {
    return plannerUri;
  }

  public void setPlannerUri(String plannerUri) {
    this.plannerUri = plannerUri;
  }

  public String getIsochroneUri() {
    return isochroneUri;
  }

  public void setIsochroneUri(String isochroneUri) {
    this.isochroneUri = isochroneUri;
  }
 
}
