package es.udc.gtfs.config;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.Transactional;

import es.udc.gtfs.model.domain.UserAuthority;
import es.udc.gtfs.model.exception.UserLoginExistsException;
import es.udc.gtfs.model.service.UserService;

@Configuration
public class DatabaseLoader {
  private final Logger logger = LoggerFactory.getLogger(DatabaseLoader.class);

  @Autowired
  private UserService userService;

  @Autowired
  private DatabaseLoader databaseLoader;

  @PostConstruct
  public void init() {
    try {
      databaseLoader.loadData();
    } catch (UserLoginExistsException e) {
      logger.error(e.getMessage(), e);
    }
  }

  @Transactional(readOnly = false, rollbackFor = Exception.class)
  public void loadData() throws UserLoginExistsException {
    userService.registerUser("pepe", "pepe", UserAuthority.ADMIN);
    userService.registerUser("maria", "maria", UserAuthority.ADMIN);
    userService.registerUser("laura", "laura", UserAuthority.MUNICIPAL);
    userService.registerUser("pedro", "pedro");

  }
}
