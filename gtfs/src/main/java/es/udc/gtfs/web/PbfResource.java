package es.udc.gtfs.web;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import es.udc.gtfs.model.exception.FileExistsException;
import es.udc.gtfs.model.exception.IllegalFileFormatException;
import es.udc.gtfs.model.service.PbfService;
import es.udc.gtfs.model.service.dto.PbfDTO;

@RestController
@RequestMapping("/api/pbf")
public class PbfResource {
    @Autowired
    private PbfService pbfService;

    @GetMapping
    public List<PbfDTO> findAll() {
        return pbfService.findAll();
    }

    @PostMapping("/importPbf")
    public void createPbf(@RequestBody MultipartFile file) throws IOException, IllegalFileFormatException, FileExistsException {
        pbfService.importPbf(file);
    }

    @PostMapping("/buildPbf")
    public void buildPbf(@RequestBody String fileName) throws IOException{
        pbfService.buildPbf(fileName);
    }

    @GetMapping("/builded")
    public String buildedPbf() {
        return pbfService.buildedPbf();
    }

    @DeleteMapping("/remove")
    public void deletePbf(@RequestBody String fileName) {
        pbfService.deletePbf(fileName);
    }
}
