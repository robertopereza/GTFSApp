package es.udc.gtfs.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import es.udc.gtfs.model.service.StopTimesService;
import es.udc.gtfs.model.service.dto.StopTimesDTO;

@RestController
@RequestMapping("/api/stoptimes")
public class StopTimesResource {
    @Autowired
    private StopTimesService stopTimesService;

    @GetMapping
    public List<StopTimesDTO> findAll() {
       return stopTimesService.findAll(); 
    }

    @GetMapping(value = "/findByStopAndRoute", params = {"stopId","routeId"})
    public List<StopTimesDTO> findByStopAndRoute(@RequestParam String stopId, @RequestParam String routeId){
        return stopTimesService.findByStopAndRoute(stopId, routeId);
    }

    @GetMapping(value = "/findByStop", params = {"stopId"})
    public List<StopTimesDTO> findByStop(@RequestParam String stopId){
        return stopTimesService.findByStop(stopId);
    }

}
