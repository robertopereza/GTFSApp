package es.udc.gtfs.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import es.udc.gtfs.model.service.RoutesService;
import es.udc.gtfs.model.service.dto.RoutesDTO;

@RestController
@RequestMapping("/api/routes")
public class RoutesResource {
    @Autowired
    private RoutesService routesService;

    @GetMapping
    public List<RoutesDTO> findAll() {
       return routesService.findAll(); 
    }

    @GetMapping(value = "/findByStop", params = "stopId")
    public List<RoutesDTO> findByStop(@RequestParam String stopId){
        return routesService.findByStop(stopId);
    }

}
