package es.udc.gtfs.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import es.udc.gtfs.model.service.StopsService;
import es.udc.gtfs.model.service.dto.StopsDTO;

@RestController
@RequestMapping("/api/stops")
public class StopsResource {
    @Autowired
    private StopsService stopsService;

    @GetMapping
    public List<StopsDTO> findAll() {
       return stopsService.findAll(); 
    }

}
