package es.udc.gtfs.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import es.udc.gtfs.model.service.ShapesService;
import es.udc.gtfs.model.service.dto.ShapesDTO;

@RestController
@RequestMapping("/api/shapes")
public class ShapesResource {
    @Autowired
    private ShapesService shapesService;

    @GetMapping
    public List<ShapesDTO> findAll() {
       return shapesService.findAll(); 
    }

    @GetMapping(value = "/findByRoute", params = "routeId")
    public List<ShapesDTO> findByRoute(@RequestParam String routeId){
        return shapesService.findByRoute(routeId);
    }

}
