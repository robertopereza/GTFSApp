package es.udc.gtfs.web;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import es.udc.gtfs.model.domain.UserAuthority;
import es.udc.gtfs.model.exception.NotFoundException;
import es.udc.gtfs.model.exception.OperationNotAllowed;
import es.udc.gtfs.model.exception.UserLoginExistsException;
import es.udc.gtfs.model.security.JWTToken;
import es.udc.gtfs.model.security.TokenProvider;
import es.udc.gtfs.model.service.UserService;
import es.udc.gtfs.model.service.dto.LoginDTO;
import es.udc.gtfs.model.service.dto.UserDTO;
import es.udc.gtfs.web.exceptions.CredentialsAreNotValidException;
import es.udc.gtfs.web.exceptions.RequestBodyNotValidException;



/**
 * Este controlador va por separado que el UserResource porque se encarga de
 * tareas relacionadas con la autenticación, registro, etc.
 *
 * <p>
 * También permite a cada usuario logueado en la aplicación obtener información
 * de su cuenta
 */
@RestController
@RequestMapping("/api")
public class AccountResource {
  private final Logger logger = LoggerFactory.getLogger(AccountResource.class);

  @Autowired
  private TokenProvider tokenProvider;

  @Autowired
  private AuthenticationManager authenticationManager;

  @Autowired
  private UserService userService;

  @PostMapping("/authenticate")
  public JWTToken authenticate(@Valid @RequestBody LoginDTO loginDTO) throws CredentialsAreNotValidException {

    UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(
        loginDTO.getLogin(), loginDTO.getPassword());
    try {
      Authentication authentication = authenticationManager.authenticate(authenticationToken);
      SecurityContextHolder.getContext().setAuthentication(authentication);
      String jwt = tokenProvider.createToken(authentication);
      return new JWTToken(jwt);
    } catch (AuthenticationException e) {
      logger.warn(e.getMessage(), e);
      throw new CredentialsAreNotValidException(e.getMessage());
    }
  }

  @GetMapping("/account")
  public UserDTO getAccount() {
    return userService.getCurrentUserWithAuthority();
  }

  @PostMapping("/register")
  public void registerAccount(@Valid @RequestBody UserDTO account, Errors errors)
      throws UserLoginExistsException, RequestBodyNotValidException {
    if (errors.hasErrors()) {
      throw new RequestBodyNotValidException(errors);
    }

    userService.registerUser(account.getLogin(), account.getPassword());
  }

  @PostMapping("/create")
  public void createAccount(@Valid @RequestBody UserDTO account, Errors errors)
      throws UserLoginExistsException, RequestBodyNotValidException {
    if (errors.hasErrors()) {
      throw new RequestBodyNotValidException(errors);
    }

    userService.registerUser(account.getLogin(), account.getPassword(), UserAuthority.valueOf(account.getAuthority()));
  }

  @PostMapping("/remove")
  public void removeAccount(@Valid @RequestBody UserDTO account, Errors errors)
      throws RequestBodyNotValidException, NotFoundException, OperationNotAllowed {
    if (errors.hasErrors()) {
      throw new RequestBodyNotValidException(errors);
    }  
    userService.removeUser(account.getId());   
  }

  @PutMapping("/update")
  public void updateAccount(@Valid @RequestBody UserDTO account, Errors errors)
      throws RequestBodyNotValidException, NotFoundException {
    if (errors.hasErrors()) {
      throw new RequestBodyNotValidException(errors);
    }  
    userService.updateUser(account.getId(), UserAuthority.valueOf(account.getAuthority()));   
  }
}
