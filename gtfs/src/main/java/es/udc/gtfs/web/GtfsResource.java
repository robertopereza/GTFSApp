package es.udc.gtfs.web;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import es.udc.gtfs.model.exception.FileExistsException;
import es.udc.gtfs.model.exception.IllegalFileFormatException;
import es.udc.gtfs.model.service.GtfsService;
import es.udc.gtfs.model.service.dto.GtfsDTO;

@RestController
@RequestMapping("/api/gtfs")
public class GtfsResource {
    @Autowired
    private GtfsService gtfsService;

    @GetMapping
    public List<GtfsDTO> findAll() {
        return gtfsService.findAll();
    }

    @PostMapping("/importGtfs")
    public void createGtfs(@RequestBody MultipartFile file) throws IOException, IllegalFileFormatException, FileExistsException {
        gtfsService.importGtfs(file);
    }

    @PostMapping("/buildGtfs")
    public void buildGtfs(@RequestBody List<String> files) throws IOException {
        gtfsService.buildGtfs(files);
    }

    @GetMapping("/builded")
    public List<String> buildedGtfs() {
        return gtfsService.buildedGtfs();
    }

    @DeleteMapping("/remove")
    public void deleteGtfs(@RequestBody String fileName) {
        gtfsService.deleteGtfs(fileName);
    }
}
