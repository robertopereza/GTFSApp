package es.udc.gtfs.web;

import java.util.List;
import java.util.concurrent.ExecutionException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import es.udc.gtfs.model.service.BadAccessStopsService;
import es.udc.gtfs.model.service.dto.BadAccessStopsDTO;


@RestController
@RequestMapping("/api/badAccessStops")
public class BadAccessStopsResource {
    @Autowired
    private BadAccessStopsService badAccessStopsService;

    @GetMapping
    public List<BadAccessStopsDTO> findAll() {
       return badAccessStopsService.findAll(); 
    }

    @GetMapping(value = "/calculateStops", params = {"meters","time"})
    public void calculateBadAccessStops(@RequestParam Integer meters, @RequestParam Integer time) throws ExecutionException, InterruptedException {
       badAccessStopsService.calculateBadAccessStops(meters, time); 
    }
}
