package es.udc.gtfs.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import es.udc.gtfs.model.service.PlannerService;

@RestController
@RequestMapping("/api/planner")
public class PlannerResource {
    @Autowired
    private PlannerService plannerService;

    @GetMapping(value = "/planRoute", params = {"start","end"}, produces="application/json")
    @ResponseBody
    public String planRoute(@RequestParam String start, @RequestParam String end){
        return plannerService.planRoute(start, end);
    }

    @GetMapping(value = "/findIsochrones", params = {"fromPlace","cutoffSec1","cutoffSec2","cutoffSec3"}, produces="application/json")
    @ResponseBody
    public String findIsochrones(@RequestParam String fromPlace, @RequestParam String cutoffSec1, @RequestParam String cutoffSec2, @RequestParam String cutoffSec3){
        return plannerService.findIsochrones(fromPlace, cutoffSec1, cutoffSec2, cutoffSec3);
    }
}
