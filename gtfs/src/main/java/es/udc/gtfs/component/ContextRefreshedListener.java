package es.udc.gtfs.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import es.udc.gtfs.model.domain.OtpProcess;

@Component
public class ContextRefreshedListener implements ApplicationListener<ContextRefreshedEvent> {
    @Autowired
    OtpProcess otpProcess;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        System.out.println("Proceso iniciado");
        if (otpProcess.isActive()) {
            System.out.println("El proceso OTP " + otpProcess.getProcess().pid() + " está activo");
        } else {
            System.out.println("El proceso OTP está inactivo");
        }
        
    }
    
}
