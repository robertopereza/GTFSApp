-- agency

CREATE TABLE IF NOT EXISTS agency(
	agency_id varchar,
	agency_name varchar PRIMARY KEY,
	agency_url varchar NOT NULL,
	agency_timezone varchar NOT NULL,
	agency_lang varchar,
	agency_phone varchar,
	agency_fare_url varchar,
	agency_email varchar
);

-- INSERT INTO agency(:agencycolumns) SELECT :agencycolumns FROM stg_agency;

-- calendar

CREATE TABLE IF NOT EXISTS calendar(
	service_id varchar PRIMARY KEY,
	monday integer NOT NULL,
	tuesday integer NOT NULL,
	wednesday integer NOT NULL,
	thursday integer NOT NULL,
	friday integer NOT NULL,
	saturday integer NOT NULL,
	sunday integer NOT NULL,
	start_date date NOT NULL,
	end_date date NOT NULL
);

-- INSERT INTO calendar(:calendarcolumns) SELECT :calendarcolumns FROM stg_calendar;

-- calendar_dates

CREATE TABLE IF NOT EXISTS calendar_dates(
	service_id varchar NOT NULL,
	date date NOT NULL,
	exception_type integer NOT NULL

);

ALTER TABLE calendar_dates DROP CONSTRAINT IF EXISTS "CALENDAR_DATES_PKEY";
ALTER TABLE calendar_dates ADD CONSTRAINT "CALENDAR_DATES_PKEY" PRIMARY KEY (service_id, date);

-- INSERT INTO calendar_dates(:calendar_datescolumns) SELECT :calendar_datescolumns FROM stg_calendar_dates;

-- fare_attributes

CREATE TABLE IF NOT EXISTS fare_attributes(
	fare_id varchar PRIMARY KEY,
	price float NOT NULL,
	currency_type varchar NOT NULL,
	payment_method integer NOT NULL,
	transfers integer NOT NULL,
	agency_id varchar,
	transfer_duration integer
);

-- INSERT INTO fare_attributes(:fare_attributescolumns) SELECT :fare_attributescolumns FROM stg_fare_attributes;

-- fare_rules

CREATE TABLE IF NOT EXISTS fare_rules(
	fare_id varchar NOT NULL,
	route_id varchar,
	origin_id varchar,
	destination_id varchar,
	contains_id varchar
);

-- INSERT INTO fare_rules(:fare_rulescolumns) SELECT :fare_rulescolumns FROM stg_fare_rules;

-- feed_info

CREATE TABLE IF NOT EXISTS feed_info(
	feed_publisher_name varchar PRIMARY KEY,
	feed_publisher_url varchar NOT NULL,
	feed_lang varchar NOT NULL,
	feed_start_date date,
	feed_end_date date,
	feed_version varchar,
	feed_contact_email varchar,
	feed_contact_url varchar
);

-- INSERT INTO feed_info(:feed_infocolumns) SELECT :feed_infocolumns FROM stg_feed_info;

-- frequencies

CREATE TABLE IF NOT EXISTS frequencies(
	trip_id varchar,
	start_time interval NOT NULL,
	end_time interval NOT NULL,
	headway_secs integer NOT NULL,
	exact_times integer
);

-- routes

CREATE TABLE IF NOT EXISTS routes(
	route_id varchar PRIMARY KEY,
	agency_id varchar,
	route_short_name varchar,
	route_long_name varchar,
	route_desc varchar,
	route_type varchar NOT NULL,
	route_url varchar,
	route_color varchar,
	route_text_color varchar,
	route_sort_order integer,
	continuous_pickup integer,
	continuous_drop_off integer
);

-- INSERT INTO routes(:routescolumns) SELECT :routescolumns FROM stg_routes;

-- shapes

CREATE TABLE IF NOT EXISTS shapes(
	shape_id varchar NOT NULL,
	shape_pt_lat varchar NOT NULL,
	shape_pt_lon varchar NOT NULL,
	shape_pt_sequence varchar NOT NULL,
	shape_dist_traveled double precision
);

ALTER TABLE shapes DROP CONSTRAINT IF EXISTS "SHAPES_PKEY";
ALTER TABLE shapes ADD CONSTRAINT "SHAPES_PKEY" PRIMARY KEY (shape_id, shape_pt_sequence);

-- INSERT INTO shapes(:shapescolumns) SELECT :shapescolumns FROM stg_shapes;

-- shapes_multiline

CREATE TABLE IF NOT EXISTS shapes_multiline(
	shape_id varchar PRIMARY KEY,
	extension geometry(linestring, 4326),
	shape_dist_traveled double precision
);

CREATE INDEX IF NOT EXISTS shapes_multiline_extension ON shapes_multiline USING gist(extension);

--INSERT INTO shapes_multiline (shape_id, extension, shape_dist_traveled)
--SELECT shape_id, ST_MAKELINE(ST_POINTFROMTEXT(CONCAT('POINT(',shape_pt_lat,' ',shape_pt_lon,')'))), SUM(shape_dist_traveled) from shapes
--GROUP BY shape_id
--ORDER BY shape_id;

-- trips

CREATE TABLE IF NOT EXISTS trips(
	route_id varchar NOT NULL,
	service_id varchar NOT NULL,
	trip_id varchar PRIMARY KEY,
	trip_headsign varchar,
	trip_short_name varchar,
	direction_id varchar,
	block_id varchar,
	shape_id varchar,
	wheelchair_accessible varchar,
	bikes_allowed varchar
);

-- INSERT INTO trips(:tripscolumns) SELECT :tripscolumns FROM stg_trips;

-- stops

CREATE TABLE IF NOT EXISTS stops(
	stop_id varchar PRIMARY KEY,
	stop_code varchar,
	stop_name varchar NOT NULL,
	stop_desc varchar,
	stop_lat varchar NOT NULL,
	stop_lon varchar NOT NULL,
	zone_id varchar,
	stop_url varchar,
	location_type varchar,
	parent_station varchar,
	stop_timezone varchar,
	wheelchair_boarding varchar,
	level_id varchar,
	platform_code varchar
);

-- INSERT INTO stops(:stopscolumns) SELECT :stopscolumns FROM stg_stops;

-- stops_point

CREATE TABLE IF NOT EXISTS stops_point(
	stop_id varchar PRIMARY KEY,
	stop_code varchar,
	stop_name varchar NOT NULL,
	stop_desc varchar,
	stop_point geometry(point, 4326) NOT NULL,
	zone_id varchar,
	stop_url varchar,
	location_type varchar,
	parent_station varchar,
	stop_timezone varchar,
	wheelchair_boarding varchar,
	level_id varchar,
	platform_code varchar
);

CREATE INDEX IF NOT EXISTS stops_point_extension ON stops_point USING gist(stop_point);

--INSERT INTO stops_point (stop_id, stop_code, stop_name, stop_desc, stop_point, zone_id, stop_url, location_type, parent_station, stop_timezone, wheelchair_boarding)
--SELECT stop_id, stop_code, stop_name, stop_desc, ST_POINTFROMTEXT(CONCAT('POINT(', stop_lat, ' ', stop_lon, ')')), zone_id, stop_url, location_type, parent_station, stop_timezone, wheelchair_boarding
--FROM stops; 

-- stop_times

CREATE TABLE IF NOT EXISTS stop_times(
	trip_id varchar NOT NULL,
	arrival_time varchar NOT NULL,
	departure_time varchar NOT NULL,
	stop_id varchar NOT NULL,
	stop_sequence varchar NOT NULL,
	stop_headsign varchar,
	pickup_type varchar,
	drop_off_type varchar,
	continuous_pickup integer,
	continuous_drop_off integer,
	shape_dist_traveled double precision,
	timepoint varchar

);

ALTER TABLE stop_times DROP CONSTRAINT IF EXISTS "STOP_TIMES_PKEY";
ALTER TABLE stop_times ADD CONSTRAINT "STOP_TIMES_PKEY" PRIMARY KEY (trip_id, arrival_time, stop_id);

-- INSERT INTO stop_times(:stop_timescolumns) SELECT :stop_timescolumns FROM stg_stop_times;

-- transfers

CREATE TABLE IF NOT EXISTS transfers(
	from_stop_id varchar NOT NULL,
	to_stop_id varchar NOT NULL,
	transfer_type varchar NOT NULL,
	min_transfer_time integer
);

-- -- INSERT INTO transfers(:transferscolumns) SELECT :transferscolumns FROM stg_transfers;

-- pathways

CREATE TABLE IF NOT EXISTS pathways(
	pathway_id varchar NOT NULL,
	from_stop_id varchar NOT NULL,
	to_stop_id varchar NOT NULL,
	pathway_mode varchar NOT NULL,
	is_bidirectional varchar NOT NULL,
	length float,
	traversal_time integer,
	stair_count integer,
	max_slope float,
	min_width float,
	signposted_as varchar,
	reversed_signposted_as varchar
);

-- -- INSERT INTO pathways(:pathwayscolumns) SELECT :pathwayscolumns FROM stg_pathways;

-- levels

CREATE TABLE IF NOT EXISTS levels(
	level_id varchar NOT NULL,
	level_index float NOT NULL,
	level_name varchar
);

-- -- INSERT INTO levels(:levelscolumns) SELECT :levelscolumns FROM stg_levels;

-- translations

CREATE TABLE IF NOT EXISTS translations(
	table_name varchar NOT NULL,
	field_name varchar NOT NULL,
	language varchar NOT NULL,
	translation varchar NOT NULL,
	record_id varchar,
	record_sub_id varchar,
	field_value varchar
);

-- -- INSERT INTO translations(:translationscolumns) SELECT :translationscolumns FROM stg_translations;

-- attributions

CREATE TABLE IF NOT EXISTS attributions(
	attribution_id varchar,
	agency_id varchar,
	route_id varchar,
	trip_id varchar,
	organization_name varchar,
	is_producer varchar,
	is_operator varchar,
	is_authority varchar,
	attribution_url varchar,
	attribution_email varchar,
	attribution_phone varchar
);

-- -- INSERT INTO attributions(:attributionscolumns) SELECT :attributionscolumns FROM stg_attributions;

-- ALTER TABLE calendar_dates ADD CONSTRAINT fk_calendar_dates_calendar FOREIGN KEY (service_id) REFERENCES calendar (service_id);
-- ALTER TABLE fare_attributes ADD CONSTRAINT fk_fare_attributes_agency FOREIGN KEY (agency_id) REFERENCES agency (agency_id);
-- ALTER TABLE routes ADD CONSTRAINT fk_routes_agency FOREIGN KEY (agency_id) REFERENCES agency (agency_id);
-- ALTER TABLE trips ADD CONSTRAINT fk_trips_routes FOREIGN KEY (route_id) REFERENCES routes (route_id);
-- ALTER TABLE trips ADD CONSTRAINT fk_trips_calendar FOREIGN KEY (service_id) REFERENCES calendar (service_id);
-- ALTER TABLE trips ADD CONSTRAINT fk_trips_shapes_multiline FOREIGN KEY (shape_id) REFERENCES shapes_multiline (shape_id);
-- ALTER TABLE stop_times ADD CONSTRAINT fk_stop_times_trips FOREIGN KEY (trip_id) REFERENCES trips (trip_id);
-- ALTER TABLE stop_times ADD CONSTRAINT fk_stop_times_stops_point FOREIGN KEY (stop_id) REFERENCES stops_point (stop_id);

CREATE INDEX IF NOT EXISTS trip_id_stop_times_idx ON stop_times(trip_id);
CREATE INDEX IF NOT EXISTS stop_id_stop_times_idx ON stop_times(stop_id);
