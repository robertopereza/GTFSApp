INSERT INTO shapes_multiline (shape_id, extension, shape_dist_traveled)
SELECT shape_id, ST_MAKELINE(ST_POINTFROMTEXT(CONCAT('POINT(',shape_pt_lat,' ',shape_pt_lon,')'))), SUM(shape_dist_traveled) from shapes
GROUP BY shape_id
ORDER BY shape_id
ON CONFLICT DO NOTHING;

INSERT INTO stops_point (stop_id, stop_code, stop_name, stop_desc, stop_point, zone_id, stop_url, location_type, parent_station, stop_timezone, wheelchair_boarding)
SELECT stop_id, stop_code, stop_name, stop_desc, ST_POINTFROMTEXT(CONCAT('POINT(', stop_lat, ' ', stop_lon, ')')), zone_id, stop_url, location_type, parent_station, stop_timezone, wheelchair_boarding
FROM stops
ON CONFLICT DO NOTHING; 
