DROP TABLE IF EXISTS stg_routes CASCADE;

CREATE TABLE stg_routes(
	route_id varchar PRIMARY KEY,
	agency_id varchar,
	route_short_name varchar,
	route_long_name varchar,
	route_desc varchar,
	route_type varchar NOT NULL,
	route_url varchar,
	route_color varchar,
	route_text_color varchar,
	continuous_drop_off integer
);